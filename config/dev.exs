import Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with esbuild to bundle .js and .css sources.
config :stepper_daemon, StepperDaemonApi.Endpoint,
  code_reloader: true,
  debug_errors: true

config :stepper_state, simulate_auth: true

config :stepper_state, StepperStateApi.Endpoint,
  code_reloader: true,
  debug_errors: true

config :stepper_gui, StepperGuiWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4200],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  secret_key_base: "9mbkwNEhHN/uGvtz4Xb3p3xfqRmDxf9B5lD6I8g5Vkknjb3az53ZijXDSfeuTPb3",
  watchers: [
    # Start the esbuild watcher by calling Esbuild.install_and_run(:default, args)
    esbuild: {Esbuild, :install_and_run, [:default, ~w(--sourcemap=inline --watch)]},
    tailwind: {Tailwind, :install_and_run, [:default, ~w(--watch)]}
  ]

# Watch static and templates for browser reloading.
config :stepper_gui, StepperGuiWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"priv/gettext/.*(po)$",
      ~r"lib/stepper_gui_web/(live|views)/.*(ex)$",
      ~r"lib/stepper_gui_web/templates/.*(eex)$"
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20
