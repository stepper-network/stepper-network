# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of the Config module.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
import Config

# Default Stepper configuration variables
config :stepper,
  data_dir: "/var/lib/stepper",
  run_dir: "/var/run/stepper"

config :stepper_daemon, StepperDaemonApi.Endpoint,
  url: [host: "localhost"],
  render_errors: [
    formats: [api: StepperDaemonApi.ErrorView],
    layout: false
  ]

config :stepper_state, StepperStateApi.Endpoint,
  url: [host: "localhost"],
  render_errors: [
    formats: [api: StepperStateApi.ErrorApi],
    layout: false
  ]

config :stepper_gui, StepperGuiWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [
    formats: [html: StepperGuiWeb.ErrorHTML],
    layout: false
  ],
  pubsub_server: StepperGui.PubSub,
  live_view: [signing_salt: "pxjocg0N"]

config :stepper_gui, StepperGui.SessionCache,
  # 2 hours
  idle_timeout_ms: 1000 * 60 * 60 * 2,
  # 2 weeks
  max_age_ms: 1000 * 60 * 60 * 24 * 7 * 2,
  # 5 minutes
  cleanup_ms: 1000 * 60 * 5,
  # 1 minute
  retry_fetch_ms: 1000 * 60,
  # 10 minutes
  reap_ms: 1000 * 60 * 10,
  # Maximum size of failed store put or delete requests queue for retrying later.
  # Going over this size will stop retrying put/delete requests since oldest is replaced.
  # This limit 'should' not ever be reached in practice so it's just there for memory safety.
  max_fail_size: 1000

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Add Stepper format encoder to easily format Stepper inter-service responses
config :phoenix_template, :format_encoders, api: Stepper.Serde.Binary.Engine

config :mime, :types, %{
  "application/octet-stream" => ["api"]
}

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.41",
  default: [
    args: ~w(
      js/app.js
      fonts/inter/inter.css
      --bundle
      --loader:.woff2=file
      --loader:.woff=file
      --target=es2017
      --outdir=../priv/static/assets
      --external:/fonts/*
      --external:/images/*
    ),
    cd: Path.expand("../apps/stepper_gui/assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configure tailwind (the version is required)
config :tailwind,
  version: "3.2.4",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../apps/stepper_gui/assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
