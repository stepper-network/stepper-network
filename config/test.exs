import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :stepper_gui, StepperGuiWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4200],
  secret_key_base: "BPF6v6Ak1gkwcRibc3qymaVnQaZ09vQVNBXq85/N6S50NuxyCp+4Wenqe3l8vSea",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# In test we don't send emails.
config :stepper, Stepper.Mailer, adapter: Swoosh.Adapters.Test

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
