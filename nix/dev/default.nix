{ beam, nix-gitignore, pname }:

let
  # force Erlang/OTP 25
  packages = beam.packagesWith beam.interpreters.erlangR25;

  # force using Elixir 14
  elixir = beam.packages.erlangR25.elixir_1_14;

  version = "0.0.0";

  # use .gitignore file to ignore copying deps/builds/etc
  src = nix-gitignore.gitignoreSourcePure ../../.gitignore ../../.;

  mixFodDeps = packages.fetchMixDeps {
    inherit src version elixir;
    
    pname = "mix-deps-${pname}";
    sha256 = "sha256-RIkKw4P5LN6uartiQKEy6FzCI+UUTKBzcbSRCKt2yho=";
  };

in packages.mixRelease {
  inherit pname version src mixFodDeps elixir;
  
  LANG = "C.UTF_8";
}