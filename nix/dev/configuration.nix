{ config, lib, pkgs, ... }: {
  config = {
    networking.hostName = "dev-vm";

    # enable fish shell
    programs.fish.enable = true;

    # add some useful system packages
    environment.systemPackages = with pkgs; [
      dig
      micro
    ];

    # make root super insecure/easy to use
    users.users.root = {
      password = "";
    };
  };
}