# StepperNetwork

## Environment Variables
### Common
`STEPPER_DATA_DIR` - Directory where Stepper applications will persist their data.  Defaults to `/var/lib/stepper`.

`STEPPER_RUN_DIR` - Directory where Stepper applicaitons will create etherial, run files such as sockets.  Defaults to `/var/run/stepper`.

## Development

### Local Development

Use curl to interact with unix sockets:

`curl -X GET --unix-socket <dev root>/.dev-fs/run/daemon.socket http://anything/<path>`

### Virtual Machine

`nix build .#dev-vm`

`chmod 644 result/nixos.qcow2`

`qemu-system-x86_64 -name dev-vm -m 2048 -enable-kvm result/nixos.qcow2`