defmodule StepperNetwork.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      deps: deps(),
      releases: [
        stepper_daemon: [
          applications: [stepper_daemon: :permanent],
          include_executables_for: [:unix]
        ],
        stepper_state: [
          applications: [stepper_state: :permanent],
          include_executables_for: [:unix]
        ],
        stepper_gui: [
          applications: [stepper_gui: :permanent],
          include_executables_for: [:unix]
        ]
      ],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  defp deps do
    [
      {:excoveralls, "~> 0.10", only: :test},
      {:phoenix, "~> 1.7.0-rc.3", override: true},
      {:phoenix_live_view, "~> 0.18.3", override: true}
    ]
  end
end
