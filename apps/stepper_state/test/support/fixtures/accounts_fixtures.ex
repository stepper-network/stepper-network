defmodule StepperState.AccountsFixtures do
  @moduledoc """
  This module defines testing helpers for system accounts.
  """

  import StepperState.GroupsFixtures
  import StepperState.UsersFixtures

  def accounts, do: {all_users(), all_groups()}

  def bobs_account, do: {user_bob(), [group_wheel(), group_users()]}
  def root_account, do: {user_root(), [group_root()]}
end
