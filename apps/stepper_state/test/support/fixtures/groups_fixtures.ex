defmodule StepperState.GroupsFixtures do
  @moduledoc """
  This module defines testing helpers for Unix groups.
  """
  alias Stepper.Unix.Group

  def group_users,
    do: %Group{
      name: "users",
      password: :shadow,
      id: 100,
      group_members: ["bob"]
    }

  def group_wheel,
    do: %Group{
      name: "wheel",
      password: :shadow,
      id: 1,
      group_members: ["bob"]
    }

  def group_root,
    do: %Group{
      name: "root",
      password: :shadow,
      id: 0,
      group_members: []
    }

  def all_groups, do: [group_root(), group_wheel(), group_users()]
end
