defmodule StepperState.UsersFixtures do
  @moduledoc """
  This module defines testing helpers for Unix users.
  """
  alias Stepper.Unix.User

  def user_root,
    do: %User{
      name: "root",
      password: :shadow,
      id: 0,
      group_id: 0,
      gecos: %User.GECOS{full_name: "System administrator"},
      home_dir: "/root",
      login_shell: "/run/current-system/sw/bin/bash"
    }

  def user_bob,
    do: %User{
      name: "bob",
      password: :shadow,
      id: 1000,
      group_id: 100,
      gecos: %User.GECOS{full_name: "Bob Roberts"},
      home_dir: "/home/bob",
      login_shell: "/run/current-system/sw/bin/fish"
    }

  def all_users, do: [user_root(), user_bob()]
end
