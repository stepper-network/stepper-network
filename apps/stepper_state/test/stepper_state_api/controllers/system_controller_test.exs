defmodule StepperStateApi.SystemControllerTest do
  use StepperStateApi.ConnCase

  import Stepper.Serde.Binary
  import StepperState.AccountsFixtures

  require Logger

  @bobs_auth %{
    username: StepperState.UsersFixtures.user_bob().name,
    password: "bObHAsAGOoDPASsWORD"
  }

  describe "auth" do
    setup [:setup_bobs_auth, :setup_accounts]

    test "authorize successfully", %{conn: conn} do
      conn = post(conn, ~p"/system/auth", serialize(@bobs_auth))
      assert api_response(conn, 200) == {:ok, bobs_account()}
    end

    test "invalid authorization", %{conn: conn} do
      conn = post(conn, ~p"/system/auth", serialize(%{username: "invalid", password: "auth"}))
      assert api_response(conn, 200) == {:error, :unauthorized}
    end

    test "missing user", %{conn: conn} do
      Mimic.stub(StepperState.Accounts, :read, fn -> {[], []} end)

      conn = post(conn, ~p"/system/auth", serialize(@bobs_auth))
      assert api_response(conn, 200) == {:error, :missing_user}
    end

    test "daemon client error", %{conn: conn} do
      Mimic.stub(StepperState.DaemonClient, :post, fn _, _ -> {:error, :test_error} end)

      conn = post(conn, ~p"/system/auth", serialize(@bobs_auth))
      assert api_response(conn, 200) == {:error, :daemon_client}
    end
  end

  describe "fetch_accounts" do
    setup [:setup_accounts]

    test "fetch successfully", %{conn: conn} do
      conn = get(conn, ~p"/system/accounts")
      assert api_response(conn, 200) == {:ok, accounts()}
    end

    test "exception fetching users", %{conn: conn} do
      Mimic.stub(StepperState.Accounts, :read, fn -> raise ArgumentError, message: "test" end)

      assert_api_error_sent(:internal_server_error, fn -> get(conn, ~p"/system/accounts") end)
    end
  end

  def setup_bobs_auth(_) do
    Mimic.stub(StepperState.DaemonClient, :post, fn
      "/system/auth", @bobs_auth -> {:ok, {:ok, :authorized}}
      "/system/auth", _ -> {:ok, {:error, :unauthorized}}
    end)

    :ok
  end

  def setup_accounts(_) do
    Mimic.stub(StepperState.Accounts, :read, fn -> accounts() end)
    :ok
  end
end
