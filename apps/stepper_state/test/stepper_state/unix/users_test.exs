defmodule StepperState.UnixUsersTest do
  use ExUnit.Case, async: true
  doctest StepperState.UnixUsers

  import StepperState.UsersFixtures

  alias StepperState.UnixUsers

  describe "read/1" do
    test "valid passwd file" do
      assert UnixUsers.read("test/stepper_state/unix/etc/passwd") == [user_bob(), user_root()]
    end

    test "bad passwd file" do
      assert_raise UnixUsers.UnixUsersException,
                   "error parsing segment 'abadstring' while reading users file 'test/stepper_state/unix/etc/badfile'",
                   fn ->
                     UnixUsers.read("test/stepper_state/unix/etc/badfile")
                   end
    end

    test "missing passwd file" do
      assert_raise UnixUsers.UnixUsersException,
                   "POSIX error enoent reading users file 'test/stepper_state/unix/etc/missing'",
                   fn ->
                     UnixUsers.read("test/stepper_state/unix/etc/missing")
                   end
    end
  end
end
