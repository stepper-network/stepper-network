defmodule StepperState.UnixGroupsTest do
  use ExUnit.Case, async: true
  doctest StepperState.UnixGroups

  import StepperState.GroupsFixtures

  alias StepperState.UnixGroups

  @test_passwd_file "test/stepper_state/unix/etc/group"

  describe "read/1" do
    test "valid group file" do
      assert UnixGroups.read(@test_passwd_file) == [group_users(), group_wheel(), group_root()]
    end

    test "missing groups file" do
      assert_raise UnixGroups.UnixGroupsException,
                   "error parsing segment 'abadstring' while reading groups file 'test/stepper_state/unix/etc/badfile'",
                   fn ->
                     UnixGroups.read("test/stepper_state/unix/etc/badfile")
                   end
    end

    test "missing group file" do
      assert_raise UnixGroups.UnixGroupsException,
                   "POSIX error enoent reading groups file 'test/stepper_state/unix/etc/missing'",
                   fn ->
                     UnixGroups.read("test/stepper_state/unix/etc/missing")
                   end
    end
  end
end
