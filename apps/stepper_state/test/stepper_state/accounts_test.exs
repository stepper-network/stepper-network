defmodule StepperState.AccountsTest do
  use ExUnit.Case, async: true

  import StepperState.AccountsFixtures
  import StepperState.UsersFixtures

  alias StepperState.Accounts

  describe "get_user/2" do
    test "existing user" do
      assert Accounts.get_user(accounts(), user_bob().name) == bobs_account()
      assert Accounts.get_user(accounts(), user_bob().id) == bobs_account()
      assert Accounts.get_user(accounts(), user_root().name) == root_account()
      assert Accounts.get_user(accounts(), user_root().id) == root_account()
    end

    test "missing user" do
      assert Accounts.get_user(accounts(), "jane") == nil
      assert Accounts.get_user(accounts(), 5432) == nil
    end
  end
end
