defmodule StepperStateApi do
  def controller do
    quote do
      use Phoenix.Controller,
        formats: [:api],
        layouts: [api: false]

      import Plug.Conn
      import Stepper.Api.Response

      unquote(verified_routes())
    end
  end

  def router do
    quote do
      use Phoenix.Router

      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
    end
  end

  def verified_routes do
    quote do
      use Phoenix.VerifiedRoutes,
        endpoint: StepperStateApi.Endpoint,
        router: StepperStateApi.Router
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/channel/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
