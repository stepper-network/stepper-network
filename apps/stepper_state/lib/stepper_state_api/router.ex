defmodule StepperStateApi.Router do
  use StepperStateApi, :router

  pipeline :api do
    plug(:put_format, "api")
  end

  scope "/", StepperStateApi do
    pipe_through([:api])

    get("/session", SessionController, :fetch)
    get("/session/all", SessionController, :fetch_all)
    post("/session", SessionController, :store)
    delete("/session", SessionController, :delete)
    delete("/session/all", SessionController, :delete_all)

    get("/system/accounts", SystemController, :fetch_accounts)
    post("/system/auth", SystemController, :auth)
  end
end
