defmodule StepperStateApi.Endpoint do
  use Phoenix.Endpoint, otp_app: :stepper_state

  require Logger

  @impl true
  def init(:supervisor, config) do
    {:ok, config}
  end

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug(Phoenix.CodeReloader)
  end

  plug(Plug.RequestId)
  plug(Plug.Telemetry, event_prefix: [:phoenix, :endpoint])

  plug(Plug.Parsers,
    parsers: [Stepper.Plug.Parsers.BinaryToTerm],
    pass: ["application/octet-stream"]
  )

  plug(Plug.MethodOverride)
  plug(Plug.Head)
  plug(StepperStateApi.Router)
end
