defmodule StepperStateApi.SystemController do
  use StepperStateApi, :controller

  require Logger

  alias StepperState.Accounts
  alias StepperState.DaemonClient

  @doc """
  Returns `{:ok, %Stepper.Unix.User{}}` | `{:error, atom()}`
  """
  def auth(conn, %{username: username, password: _} = auth) do
    unless Application.get_env(:stepper_state, :simulate_auth, false) do
      with {:ok, {:ok, :authorized}} <- DaemonClient.post("/system/auth", auth),
           {_user, _groups} = user_data <- Accounts.read() |> Accounts.get_user(username) do
        send_api_resp(conn, {:ok, user_data})
      else
        {:ok, daemon_resp} ->
          send_api_resp(conn, daemon_resp)

        {:error, reason} ->
          Logger.error("Daemon client auth error: #{reason}")
          send_api_resp(conn, {:error, :daemon_client})

        nil ->
          send_api_resp(conn, {:error, :missing_user})
      end
    else
      send_api_resp(conn, {:ok, simulated_user()})
    end
  end

  @doc """
  Fetches the system users and groups.
  """
  def fetch_accounts(conn, _params) do
    send_api_resp(conn, {:ok, Accounts.read()})
  end

  defp simulated_user(),
    do:
      {%Stepper.Unix.User{
         name: "simulated",
         password: :none,
         id: 1234,
         group_id: 1234,
         gecos: %Stepper.Unix.User.GECOS{full_name: "Simulated User"},
         home_dir: "/dev/null",
         login_shell: "/sbin/nologin"
       }, [%Stepper.Unix.Group{name: "simulated", password: :none, id: 1234}]}
end
