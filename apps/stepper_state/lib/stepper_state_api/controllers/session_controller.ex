defmodule StepperStateApi.SessionController do
  use StepperStateApi, :controller

  alias Stepper.Session
  alias StepperState.Sessions

  def fetch(conn, %{"token" => token}), do: fetch(conn, %{token: token})
  def fetch(conn, %{token: token}), do: send_api_resp(conn, Sessions.fetch(token))

  def fetch_all(conn, _params), do: send_api_resp(conn, Sessions.fetch_all())

  def store(conn, %{session: %Session{} = session}),
    do: send_api_resp(conn, Sessions.put(session))

  def store(conn, %{sessions: [%Session{}] = sessions}),
    do: send_api_resp(conn, Sessions.put(sessions))

  def delete(conn, %{"token" => token}), do: delete(conn, %{token: token})

  def delete(conn, %{token: token}),
    do: send_api_resp(conn, Sessions.delete(token))

  def delete(conn, %{tokens: tokens}) when is_list(tokens),
    do: send_api_resp(conn, Sessions.delete(tokens))

  def delete_all(conn, _params), do: send_api_resp(conn, Sessions.delete_all())
end
