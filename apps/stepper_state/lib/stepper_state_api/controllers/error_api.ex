defmodule StepperStateApi.ErrorApi do
  def render(template, _assigns) do
    {:error, status_atom_from_template(template)}
  end

  defp status_atom_from_template(template) do
    template
    |> String.split(".")
    |> hd()
    |> String.to_integer()
    |> Plug.Conn.Status.reason_atom()
  rescue
    _ -> :internal_server_error
  end
end
