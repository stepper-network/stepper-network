defmodule StepperState.Accounts do
  alias Stepper.Unix.Group
  alias Stepper.Unix.User
  alias StepperState.UnixGroups
  alias StepperState.UnixUsers

  @type t() :: {[User.t()], [Group.t()]}

  @doc """
  Reads the /etc/passwd and /etc/group file and returns the users and groups.
  """
  @spec read() :: t()
  def read(), do: {UnixUsers.read(), UnixGroups.read()}

  @doc """
  Gets a user by id or name.  Returns the user and the groups the user is in.
  """
  @spec get_user(t(), integer | binary) :: {User.t(), [Group.t()]} | nil
  def get_user(accounts, id) when is_integer(id), do: filter_user(accounts, &(&1.id == id))
  def get_user(accounts, name) when is_binary(name), do: filter_user(accounts, &(&1.name == name))

  defp filter_user({users, groups}, match_user) when is_function(match_user) do
    if user = Enum.find(users, match_user) do
      groups =
        Enum.filter(groups, fn g ->
          g.name == user.name || Enum.member?(g.group_members, user.name)
        end)

      {user, groups}
    end
  end
end
