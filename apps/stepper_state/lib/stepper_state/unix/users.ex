defmodule StepperState.UnixUsers do
  defmodule UnixUsersException do
    defexception [:message]
  end

  require Logger

  alias StepperState.UnixUserParser

  @default_path "/etc/passwd"

  @doc """
  Lists the users on the system.  Uses the /etc/passwd file by default.
  """
  @spec read(binary) :: list
  def read(path \\ @default_path) when is_binary(path) do
    with {:ok, passwd} <- File.read(path),
         {:ok, users} <- parse(passwd) do
      users
    else
      {:error, posix} when is_atom(posix) ->
        raise UnixUsersException, message: "POSIX error #{posix} reading users file '#{path}'"

      {:error, seg} when is_binary(seg) ->
        raise UnixUsersException,
          message: "error parsing segment '#{seg}' while reading users file '#{path}'"
    end
  end

  @spec parse(binary) :: {:ok, list} | {:error, binary}
  defp parse(str) do
    str
    |> String.split("\n", trim: true)
    |> Enum.map(&UnixUserParser.parse/1)
    |> Enum.reduce_while({:ok, []}, fn res, {:ok, acc} ->
      case res do
        {:ok, user} -> {:cont, {:ok, [user | acc]}}
        {:error, segment} -> {:halt, {:error, segment}}
      end
    end)
  end
end
