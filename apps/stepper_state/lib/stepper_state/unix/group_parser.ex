defmodule StepperState.UnixGroupParser do
  alias Stepper.Unix.Group

  @doc """
  Creates a group by parsing a line of the /etc/group file.any()

  Format definition: https://man7.org/linux/man-pages/man5/group.5.html

  ## Examples:

    iex> StepperState.UnixGroupParser.parse("group:x:1234:")
    {:ok, %Stepper.Unix.Group{
      name: "group",
      password: :shadow,
      id: 1234,
      group_members: []
    }}

    iex> StepperState.UnixGroupParser.parse("group:x:1234:user1,user2")
    {:ok, %Stepper.Unix.Group{
      name: "group",
      password: :shadow,
      id: 1234,
      group_members: ["user1", "user2"]
    }}

    iex> StepperState.UnixGroupParser.parse("a bad string")
    {:error, "a bad string"}
  """
  @spec parse(binary) :: {:ok, Group.t()} | {:error, binary}
  def parse(str) when is_binary(str) do
    with [name, password, id, group_members] <- String.split(str, ":"),
         password <- parse_password(password),
         {id, ""} <- Integer.parse(id),
         group_members <- String.split(group_members, ",", trim: true) do
      {:ok,
       %Group{
         name: name,
         password: password,
         id: id,
         group_members: group_members
       }}
    else
      _ -> {:error, str}
    end
  end

  defp parse_password(str) do
    case str do
      "" -> :none
      "x" -> :shadow
      _ -> :set
    end
  end
end
