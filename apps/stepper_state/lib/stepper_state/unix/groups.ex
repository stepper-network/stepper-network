defmodule StepperState.UnixGroups do
  defmodule UnixGroupsException do
    defexception [:message]
  end

  alias Stepper.Unix.Group
  alias StepperState.UnixGroupParser

  @doc """
  Reads the /etc/group formatted file and returns the list of groups.
  """
  @spec read(binary) :: [Group.t()]
  def read(path \\ "/etc/group") do
    with {:ok, group_file} <- File.read(path),
         {:ok, groups} <- parse(group_file) do
      groups
    else
      {:error, posix} when is_atom(posix) ->
        raise UnixGroupsException, message: "POSIX error #{posix} reading groups file '#{path}'"

      {:error, seg} when is_binary(seg) ->
        raise UnixGroupsException,
          message: "error parsing segment '#{seg}' while reading groups file '#{path}'"
    end
  end

  @spec parse(binary) :: {:ok, list} | {:error, binary}
  defp parse(str) do
    str
    |> String.split("\n", trim: true)
    |> Enum.map(&UnixGroupParser.parse/1)
    |> Enum.reduce_while({:ok, []}, fn res, {:ok, acc} ->
      case res do
        {:ok, user} -> {:cont, {:ok, [user | acc]}}
        {:error, seg} -> {:halt, {:error, seg}}
      end
    end)
  end
end
