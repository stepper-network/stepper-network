defmodule StepperState.UnixUserParser do
  alias Stepper.Unix.User

  @doc """
  Creates a user by parsing a line in the Linux passwd file.

  ## Examples:

    iex> StepperState.UnixUserParser.parse("user:x:1234:5678::/home/user:/bin/sh")
    {:ok, %Stepper.Unix.User{
      name: "user",
      password: :shadow,
      id: 1234,
      group_id: 5678,
      gecos: %Stepper.Unix.User.GECOS{},
      home_dir: "/home/user",
      login_shell: "/bin/sh"
    }}

    iex> StepperState.UnixUserParser.parse("a bad string")
    {:error, "a bad string"}
  """
  @spec parse(binary) :: {:ok, User.t()} | {:error, binary}
  def parse(str) when is_binary(str) do
    with {:ok, [name, password, id, group_id, gecos, home_dir, login_shell]} <-
           parse_user_params(str),
         {:ok, gecos} <- parse_gecos(gecos) do
      {:ok,
       %User{
         name: name,
         password: password,
         id: id,
         group_id: group_id,
         gecos: gecos,
         home_dir: home_dir,
         login_shell: login_shell
       }}
    else
      {:error, segment} ->
        {:error, segment}

      _ ->
        {:error, str}
    end
  end

  defp parse_password(str) do
    case str do
      "" -> :none
      "x" -> :shadow
      _ -> :set
    end
  end

  defp parse_user_params(str) do
    with [name, password, id, group_id, gecos, home_dir, login_shell] <-
           String.split(str, ":"),
         password <- parse_password(password),
         {id, ""} <- Integer.parse(id),
         {group_id, ""} <- Integer.parse(group_id) do
      {:ok, [name, password, id, group_id, gecos, home_dir, login_shell]}
    else
      _ -> {:error, str}
    end
  end

  @doc """
  Creates a GECOS by parsing the gecos field in the Linux passwd file.any()

  ## Examples:

    iex> StepperState.UnixUserParser.parse_gecos("")
    {:ok, %Stepper.Unix.User.GECOS{
      full_name: "",
      location: "",
      work_phone: "",
      home_phone: "",
      other: ""
    }}

    iex> StepperState.UnixUserParser.parse_gecos("Full Name")
    {:ok, %Stepper.Unix.User.GECOS{
      full_name: "Full Name",
      location: "",
      work_phone: "",
      home_phone: "",
      other: ""
    }}

    iex> StepperState.UnixUserParser.parse_gecos("Full Name,Location,123-456-7890,987-654-3210,Other")
    {:ok, %Stepper.Unix.User.GECOS{
      full_name: "Full Name",
      location: "Location",
      work_phone: "123-456-7890",
      home_phone: "987-654-3210",
      other: "Other"
    }}

    iex> StepperState.UnixUserParser.parse_gecos("a,bad,string")
    {:error, "a,bad,string"}
  """
  def parse_gecos(str) when is_binary(str) do
    case String.split(str, ",") do
      [""] ->
        {:ok, %User.GECOS{}}

      [full_name] ->
        {:ok, %User.GECOS{full_name: full_name}}

      [full_name, location, work_phone, home_phone, other] ->
        {:ok,
         %User.GECOS{
           full_name: full_name,
           location: location,
           work_phone: work_phone,
           home_phone: home_phone,
           other: other
         }}

      _ ->
        {:error, str}
    end
  end
end
