defmodule StepperState.Sessions do
  @moduledoc """
  Module that stores the session files to disk.

  All files are stored in the configured directory.  Sessions are stored by a
  hash of their token for easy lookup.

  We may want to expore making the client calls async.  This will require some thinking and
  research into filesystems.  However, Stepper doesn't have that many sessions so this should be
  unnecessary.
  """
  use GenServer

  require Logger

  alias Stepper.Serde.Binary
  alias Stepper.Session
  alias Stepper.Unix.FileSystem

  @extension ".session"

  @type t() :: %__MODULE__{dir: binary}

  @enforce_keys [:dir]
  defstruct @enforce_keys

  def start_link(data_dir, opts \\ []) do
    opts = opts |> Keyword.put(:data_dir, data_dir)

    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl true
  def init(opts) do
    data_dir = Keyword.fetch!(opts, :data_dir)
    session_dir = FileSystem.init_subdir(data_dir, "sessions")

    {:ok, %__MODULE__{dir: session_dir}}
  end

  @doc """
  Fetches a session from a token string.  Deletes session if expired.
  """
  @spec fetch(binary) :: {:ok, Session.t()} | :error
  def fetch(token), do: GenServer.call(__MODULE__, {:fetch, token})

  @doc """
  Fetches all sessions.  Deletes sessions if expired.
  """
  @spec fetch_all() :: {:ok, [Session.t()]}
  def fetch_all(), do: GenServer.call(__MODULE__, :fetch_all)

  @doc """
  Puts the session into the filesystem store.  Returns a list of failed put tokens.
  """
  @spec put(Session.t() | [Session.t()]) :: :ok | {:error, [binary]}
  def put(%Session{} = session), do: put([session])
  def put([%Session{}] = sessions), do: GenServer.call(__MODULE__, {:put, sessions})

  @doc """
  Deletes a session by token.  Returns a list of failed deletion tokens.
  """
  @spec delete(binary | [binary]) :: :ok | {:error, [binary]}
  def delete(token) when is_binary(token), do: delete([token])
  def delete(tokens) when is_list(tokens), do: GenServer.call(__MODULE__, {:delete, tokens})

  @doc """
  Deletes all stored sessions.
  """
  @spec delete_all() :: {:ok, [binary]}
  def delete_all(), do: GenServer.call(__MODULE__, :delete_all)

  @impl true
  def handle_call({:fetch, token}, _from, %{dir: dir} = state),
    do: {:reply, read_session_file(token, dir), state}

  @impl true
  def handle_call(:fetch_all, _from, %{dir: dir} = state) do
    sessions =
      "#{dir}/*#{@extension}"
      |> Path.wildcard()
      |> Enum.map(&read_session_file(&1))
      |> Enum.filter(&match?({:ok, _}, &1))
      |> Enum.map(fn {:ok, session} -> session end)

    {:reply, {:ok, sessions}, state}
  end

  @impl true
  def handle_call({:put, [%Session{}] = sessions}, _from, %{dir: dir} = state) do
    failures =
      sessions
      |> Enum.map(&{write_session_file(&1, dir), &1})
      |> Enum.filter(&match?({:error, _session}, &1))
      |> Enum.map(fn {:error, session} -> session.token end)

    {:reply, failure_tokens_response(failures), state}
  end

  @impl true
  def handle_call({:delete, tokens}, _from, %{dir: dir} = state) when is_list(tokens) do
    failures =
      tokens
      |> Enum.map(&{session_path(&1, dir), &1})
      |> Enum.map(fn {path, token} -> {delete_session_file(path), token} end)
      |> Enum.filter(&match?({:error, _token}, &1))
      |> Enum.map(fn {:error, token} -> token end)

    {:reply, failure_tokens_response(failures), state}
  end

  @impl true
  def handle_call(:delete_all, _from, %{dir: dir} = state) do
    removed_sessions =
      "#{dir}/*#{@extension}"
      |> Path.wildcard()
      |> Enum.map(&read_session_file/1)
      |> Enum.filter(&match?({:ok, _}, &1))
      |> Enum.map(fn {:ok, session} -> {delete_session_file(session.token, dir), session} end)
      |> Enum.filter(&match?({:ok, _}, &1))
      |> Enum.map(fn {:ok, session} -> session end)

    {:reply, removed_sessions, state}
  end

  defp read_session_file(token, dir), do: session_path(token, dir) |> read_session_file()

  defp read_session_file(path) do
    with {:ok, session} <- File.read(path),
         {:ok, %Session{} = session} <- Binary.deserialize(session) do
      unless Session.expired?(session) do
        {:ok, session}
      else
        Logger.info("Fetched session #{path} expired")
        delete_session_file(path)
        :error
      end

      {:ok, session}
    else
      _ ->
        Logger.error("Bad token file at path #{path}")
        :error
    end
  end

  defp write_session_file(session, dir) do
    path = session_path(session.token, dir)
    se_session = Binary.serialize(session)

    with {:error, posix} <- File.write(path, se_session) do
      Logger.error("Failed to write session file #{path}, #{posix}")
      :error
    end
  end

  defp delete_session_file(token, dir), do: session_path(token, dir) |> delete_session_file()

  defp delete_session_file(path) do
    case File.rm(path) do
      :ok ->
        Logger.info("Removed session file #{path}")
        :ok

      {:error, posix} ->
        Logger.error("Could not remove session file #{path}, #{posix}")
        :error
    end
  end

  defp session_path(token, dir), do: Path.join(dir, session_filename(token))

  defp session_filename(token) do
    :sha256
    |> :crypto.hash(token)
    |> Base.encode64(padding: false)
    |> String.replace(["/", "+"], "")
    |> then(&"#{&1}#{@extension}")
  end

  defp failure_tokens_response(failure_tokens) do
    if Enum.any?(failure_tokens) do
      {:error, failure_tokens}
    else
      :ok
    end
  end
end
