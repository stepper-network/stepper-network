defmodule StepperState.Application do
  @moduledoc false

  use Application

  require Logger

  alias Stepper.Unix

  @impl true
  def start(_type, _args) do
    Logger.notice("Starting Stepper State application...")
    Logger.info("Initializing filesystem...")
    data_dir = Unix.FileSystem.init_data_dir(:state)
    socket_path = Unix.DomainSocket.prepare!(:state)

    children = [
      StepperState.DaemonClient,
      {StepperState.Sessions, data_dir},
      {StepperStateApi.Endpoint, http: [ip: {:local, socket_path}, port: 0]}
    ]

    opts = [strategy: :one_for_one, name: StepperState.Supervisor]

    Logger.info("Starting supervisor...")
    sup = Supervisor.start_link(children, opts)

    if Phoenix.Endpoint.server?(:stepper_state, StepperStateApi.Endpoint) do
      Unix.DomainSocket.update_permissions!(socket_path)
    end

    Logger.info("Stared Stepper State")
    sup
  end
end
