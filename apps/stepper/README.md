# Stepper

The main library for all Stepper applications.  This is the centralized coupling for the Stepper codebase.  Work done here affects all other projects.

## Important Modules

  - [`Stepper.Api.Client`](lib/api/client.ex)
  - [`Stepper.Api.Response`](lib/api/response.ex)

## Installation

```elixir
def deps do
  [
    {:stepper, in_umbrella: true}
  ]
end
```

