defmodule Stepper.Plug.Parsers.BinaryToTerm do
  @behaviour Plug.Parsers

  @impl true
  def init(opts) do
    {body_reader, opts} = Keyword.pop(opts, :body_reader, {Plug.Conn, :read_body, []})
    {body_reader, opts}
  end

  @impl true
  def parse(conn, "application", "octet-stream", _headers, {{mod, fun, args}, opts}) do
    apply(mod, fun, [conn, opts | args]) |> decode()
  end

  def parse(conn, _type, _subtype, _headers, _opts) do
    {:next, conn}
  end

  defp decode({:ok, "", conn}) do
    {:ok, %{}, conn}
  end

  defp decode({:ok, body, conn}) do
    case Stepper.Serde.Binary.deserialize(body) do
      {:ok, nil} -> {:ok, %{}, conn}
      {:ok, %{} = term} -> {:ok, term, conn}
      {:error, reason} -> raise Plug.Parsers.ParseError, exception: reason
    end
  end

  defp decode({:more, _, conn}) do
    {:error, :too_large, conn}
  end

  defp decode({:error, :timeout}) do
    raise Plug.TimeoutError
  end

  defp decode({:error, _}) do
    raise Plug.BadRequestError
  end
end
