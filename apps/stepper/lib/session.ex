defmodule Stepper.Session do
  @rand_size 96

  @enforce_keys [:token, :data, :created_at, :expires_at]
  defstruct @enforce_keys

  @type t() :: %__MODULE__{
          token: binary,
          data: map,
          created_at: integer,
          expires_at: integer
        }

  @spec build(map, integer) :: t()
  def build(data, max_age),
    do: build(:crypto.strong_rand_bytes(@rand_size) |> Base.encode64(), data, max_age)

  @spec build(binary, map, integer) :: t()
  def build(token, data, max_age) do
    current = now()

    %__MODULE__{
      token: token,
      data: data,
      created_at: current,
      expires_at: current + max_age
    }
  end

  @spec expired?(t()) :: bool
  def expired?(%__MODULE__{expires_at: expires_at}) do
    expires_at < now()
  end

  @spec now() :: integer
  def now(), do: System.system_time(:millisecond)
end
