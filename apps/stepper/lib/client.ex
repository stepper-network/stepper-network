defmodule Stepper.Client do
  @moduledoc """
  Mint client related functions for Stepper applications.
  """
  require Logger

  @doc """
  Receives a response from a Mint connection for a particular reference.

  Returns a map with the following keys:
    - `:status` - HTTP status code returned
    - `:headers` - HTTP response headers
    - `:data` - Raw HTTP response body
  """
  @spec receive_response(Mint.HTTP1.t(), Mint.Types.request_ref(), integer()) ::
          {:ok, Mint.HTTP1.t(), map()} | {:error, Mint.HTTP1.t(), atom()}
  def receive_response(mint_conn, ref, timeout) do
    with {:ok, mint_conn, entries} <- Mint.HTTP1.recv(mint_conn, 0, timeout),
         {:ok, response} <- response_to_map(entries, ref, %{}, timeout) do
      {:ok, mint_conn, response}
    else
      {:error, mint_conn, error, _entries} ->
        Logger.error("Mint receive error: " <> Exception.message(error))
        {:error, mint_conn, :http_receive}

      {:error, reason} when is_atom(reason) ->
        {:error, mint_conn, reason}
    end
  end

  defp response_to_map([entry | entries], ref, response, timeout) do
    case entry do
      {kind, ^ref, value} when kind in [:status, :headers] ->
        response = Map.put(response, kind, value)
        response_to_map(entries, ref, response, timeout)

      {:data, ^ref, data} ->
        response = Map.update(response, :data, data, &(&1 <> data))
        response_to_map(entries, ref, response, timeout)

      {:done, ^ref} ->
        {:ok, response}

      {:error, ^ref, error} ->
        Logger.error("Mint response entry error: " <> Exception.message(error))
        {:error, :response_entry}
    end
  end
end
