defmodule Stepper.Unix.User do
  @moduledoc """
  A Linux system user as defined in the `/etc/passwd` file.

  The `:password` field will always be an atom in `[:none, :shadow, :set]` (i.e. even if the
  password is set in the file (unlikely), it won't be visible through this class).
  """

  require Logger

  @enforce_keys [:name, :password, :id, :group_id]
  defstruct [
    :name,
    :password,
    :id,
    :group_id,
    gecos: %__MODULE__.GECOS{},
    home_dir: "",
    login_shell: ""
  ]

  @type t() :: %__MODULE__{
          name: String.t(),
          id: integer,
          group_id: integer,
          gecos: __MODULE__.GECOS.t(),
          home_dir: String.t(),
          login_shell: String.t()
        }
end
