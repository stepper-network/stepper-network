defmodule Stepper.Unix.DomainSocket do
  defmodule SocketError do
    defexception [:message]
  end

  alias Stepper.Unix.FileSystem

  @type service_or_path() :: :daemon | :state | binary()

  @spec path(service_or_path()) :: binary()
  def path(:daemon), do: path("daemon.socket")
  def path(:state), do: path("state.socket")

  def path(service) when is_atom(service) do
    raise SocketError, "unknown Stepper service #{service}"
  end

  def path(filename) when is_binary(filename) do
    FileSystem.get_run_dir()
    |> Path.join(filename)
  end

  def prepare!(service) when is_atom(service), do: service |> path() |> prepare!()

  def prepare!(path) do
    case File.rm(path) do
      :ok ->
        path

      {:error, :enoent} ->
        path

      {:error, reason} ->
        raise SocketError, "could not delete socket file #{path}, #{reason}"
    end
  end

  def update_permissions!(path) do
    case File.chmod(path, 0o660) do
      :ok ->
        path

      {:error, reason} ->
        raise SocketError, "could not set up permissions on #{path}, #{reason}"
    end
  end
end
