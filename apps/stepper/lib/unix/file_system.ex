defmodule Stepper.Unix.FileSystem do
  require Logger

  @doc """
  Initializes a Stepper application directory.  Returns the initialized directory path string.

  The `app_name` argument can be `:daemon`, `:state`, or any string.
  """
  @spec init_data_dir(atom | binary) :: binary
  def init_data_dir(:daemon), do: init_data_dir("daemon")
  def init_data_dir(:state), do: init_data_dir("state")

  def init_data_dir(app_name) when is_binary(app_name) do
    dir = Path.join(get_data_dir(), app_name)

    init_dir(dir, 0o750)
    Logger.info("Application data directory #{dir} initialized")

    dir
  end

  def init_subdir(app_dir, subdir, opts \\ []) do
    subdir = Path.join(app_dir, subdir)

    mode =
      if Keyword.get(opts, :private, false) do
        0o700
      else
        0o750
      end

    init_dir(subdir, mode)
    Logger.info("Application subdirectory #{subdir} initialized")

    subdir
  end

  def get_run_dir do
    case System.fetch_env("STEPPER_RUN_DIR") do
      {:ok, data_dir} -> data_dir
      _ -> Application.fetch_env!(:stepper, :run_dir)
    end
  end

  def get_data_dir do
    case System.fetch_env("STEPPER_DATA_DIR") do
      {:ok, data_dir} -> data_dir
      _ -> Application.fetch_env!(:stepper, :data_dir)
    end
  end

  defp init_dir(dir, mode) do
    if File.exists?(dir) do
      unless File.dir?(dir) do
        raise "Initialization of directory failed, #{dir} is not a directory"
      end
    else
      File.mkdir!(dir)
    end

    File.chmod!(dir, mode)
  end
end
