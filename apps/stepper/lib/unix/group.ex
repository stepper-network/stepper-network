defmodule Stepper.Unix.Group do
  @moduledoc """
  Struct/type module for Unix groups defined in the `/etc/group` file
  """

  @enforce_keys [:name, :password, :id]
  defstruct [
    :name,
    :password,
    :id,
    group_members: []
  ]

  @type t() :: %__MODULE__{
          name: String.t(),
          password: atom,
          id: integer,
          group_members: [String.t()]
        }
end
