defmodule Stepper.Unix.Auth do
  @moduledoc """
  Authentication struct with creation helpers.
  """

  @enforce_keys [:username, :password]
  defstruct [
    :username,
    :password
  ]

  @typedoc "A UTF-8 encoded authentiation stuct."
  @type t() :: %__MODULE__{
          username: String.t(),
          password: String.t()
        }

  @doc """
  Creates authentication struct from username and password.

  ## Examples

    iex> Stepper.Unix.Auth.new("user", "password")
    %Stepper.Unix.Auth{username: "user", password: "password"}
  """
  @spec new(String.t(), String.t()) :: t()
  def new(username, password) do
    %__MODULE__{
      username: username,
      password: password
    }
  end
end
