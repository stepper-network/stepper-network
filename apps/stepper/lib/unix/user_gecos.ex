defmodule Stepper.Unix.User.GECOS do
  defstruct full_name: "",
            location: "",
            work_phone: "",
            home_phone: "",
            other: ""

  @type t() :: %__MODULE__{
          full_name: String.t(),
          location: String.t(),
          work_phone: String.t(),
          home_phone: Stirng.t(),
          other: String.t()
        }
end
