defmodule Stepper.Test.Api do
  import ExUnit.Assertions, only: [flunk: 1]

  alias Plug.Conn
  alias Stepper.Serde.Binary

  require Logger

  @spec api_response(Conn.t(), status :: integer() | atom()) :: term()
  def api_response(%Conn{status: status, resp_body: body}, given) do
    if given == status do
      if body do
        {:ok, resp} = Binary.deserialize(body)
        resp
      else
        raise "api responses from Stepper require a body"
      end
    else
      raise "expected response with status #{given}, got: #{status}, with body:\n#{inspect(body)}"
    end
  end

  @spec assert_api_error_sent(integer() | atom(), function()) :: term()
  def assert_api_error_sent(status_int_or_atom, func) do
    expected_status = Conn.Status.code(status_int_or_atom)
    discard_previously_sent()

    result =
      func
      |> wrap_request()
      |> receive_response(expected_status)

    discard_previously_sent()
    result
  end

  defp receive_response({:ok, conn}, expected_status) do
    if conn.state == :sent do
      flunk(
        "expected error to be sent as #{expected_status} status, but response sent #{conn.status} without error"
      )
    else
      flunk("expected error to be sent as #{expected_status} status, but no error happened")
    end
  end

  defp receive_response({:error, {_kind, exception, stack}}, expected_status) do
    receive do
      {ref, {^expected_status, _headers, body}} when is_reference(ref) ->
        with {:ok, {:error, reason}} <- Binary.deserialize(body) do
          reason
        else
          {:error, message} ->
            flunk(
              "expected error to be sent in Stepper binary format, but could not be deserialized: #{message}"
            )

          {:ok, nonerror} ->
            flunk("expected Stepper formatted error result, got #{inspect(nonerror)}")
        end

      {ref, {sent_status, _headers, _body}} when is_reference(ref) ->
        reraise ExUnit.AssertionError.exception("""
                expected error to be sent as #{expected_status} status, but got #{sent_status} from:
                #{Exception.format_banner(:error, exception)}
                """),
                stack
    after
      0 ->
        reraise ExUnit.AssertionError.exception("""
                expected error to be sent as #{expected_status} status, but got an error with no response from:
                #{Exception.format_banner(:error, exception)}
                """),
                stack
    end
  end

  defp discard_previously_sent() do
    receive do
      {ref, {_, _, _}} when is_reference(ref) -> discard_previously_sent()
      {:plug_conn, :sent} -> discard_previously_sent()
    after
      0 -> :ok
    end
  end

  defp wrap_request(func) do
    try do
      {:ok, func.()}
    catch
      kind, error -> {:error, {kind, error, __STACKTRACE__}}
    end
  end
end
