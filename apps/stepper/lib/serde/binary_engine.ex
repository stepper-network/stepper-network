defmodule Stepper.Serde.Binary.Engine do
  @moduledoc """
  This module defines how Stepper API Phoenix handlers encode the body of the HTTP response.any()

  Stepper APIs are expected to return data as defined in the `Stepper.Result` module.  This module
  will throw if the result data is not in the proper format.
  """

  alias Stepper.Api.Response

  @spec encode_to_iodata!(Result.t()) :: binary
  def encode_to_iodata!(result), do: Response.encode!(result)
end
