defmodule Stepper.Serde.Binary do
  @moduledoc """
  Opinionated serializer/deserializer using Erlang's term_to_binary/2 and binary_to_term/2 functions.
  """

  defmodule DeserializeError do
    defexception [:message]
  end

  @doc """
  Serializes the term into an Erlang binary (version 2).

  ## Examples

    iex> Stepper.Serde.Binary.serialize("test")
    <<131, 109, 0, 0, 0, 4, 116, 101, 115, 116>>
  """
  @spec serialize(term) :: binary
  def serialize(term) do
    :erlang.term_to_binary(term, minor_version: 2)
  end

  @doc """
  Deserializes the Erlang binary into a safe term.  Returns :error if unable to do so.

  ## Examples

    iex> Stepper.Serde.Binary.deserialize(<<131, 109, 0, 0, 0, 4, 116, 101, 115, 116>>)
    {:ok, "test"}

    iex> Stepper.Serde.Binary.deserialize("test")
    :error
  """
  @spec deserialize(binary) :: {:ok, term} | :error
  def deserialize(binary) when is_binary(binary) do
    try do
      term = :erlang.binary_to_term(binary)
      {:ok, term}
    rescue
      _ -> :error
    end
  end
end
