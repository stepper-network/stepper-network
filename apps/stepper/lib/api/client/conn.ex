defmodule Stepper.Api.Client.Conn do
  @moduledoc """
  Connection used by the Stepper clients.

  Inspired heavily by the Finch library: https://github.com/sneako/finch.
  """
  require Logger

  alias Stepper.Api.Request
  alias Stepper.Client
  alias Stepper.Serde.Binary

  # These headers are required for Stepper clients to respond successfully.
  @headers [
    {"host", "localhost"},
    {"content-type", "application/octet-stream"}
  ]

  @type conn() :: %{
          host: {:local, binary},
          opts: keyword(),
          parent: pid(),
          last_checkin: integer(),
          mint: Mint.HTTP.t() | nil
        }

  @doc """
  Creates a new Stepper client connection.

  Initializes with empty Mint connection for lazy connections.
  """
  @spec new(binary(), pid(), keyword()) :: conn()
  def new(socket_path, parent, opts) do
    %{
      host: {:local, socket_path},
      opts: opts,
      parent: parent,
      last_checkin: System.monotonic_time(),
      mint: nil
    }
  end

  @doc """
  Connects to the Unix domain socket and adds the Mint connection to the client connection map.

  Leaves it up for the caller to decide what do do on failure.
  """
  @spec connect(conn()) :: {:ok, conn()} | {:error, conn(), atom()}
  def connect(%{mint: mint} = conn) when not is_nil(mint), do: {:ok, conn}

  def connect(%{mint: nil} = conn) do
    opts = Keyword.merge(conn.opts, mode: :passive, protocols: [:http1], hostname: "localhost")

    case Mint.HTTP1.connect(:http, conn.host, 0, opts) do
      {:ok, mint} ->
        {:ok, %{conn | mint: mint}}

      {:error, error} when is_exception(error) ->
        Logger.error("API client connection error: " <> Exception.message(error))
        {:error, conn, :client_connect}
    end
  end

  @spec request(conn(), binary(), binary(), Request.t(), integer()) ::
          {:ok, conn(), Response.t()} | {:error, conn(), atom()}
  def request(%{mint: nil} = conn, _method, _path, _body, _receive_timeout),
    do: {:error, conn, :not_connected}

  def request(conn, method, path, nil, receive_timeout),
    do: request(conn, method, path, %{}, receive_timeout)

  def request(conn, method, path, body, receive_timeout) do
    with se_body <- Request.encode!(body),
         {:ok, mint, ref} <- Mint.HTTP1.request(conn.mint, method, path, @headers, se_body),
         {:ok, mint, resp} <- Client.receive_response(mint, ref, receive_timeout),
         conn <- %{conn | mint: mint} do
      case process_response(resp) do
        {:ok, data} -> {:ok, conn, data}
        {:error, reason} -> {:error, conn, reason}
      end
    else
      {:error, mint, error} when is_exception(error) ->
        Logger.error("API client connection request error: " <> Exception.message(error))
        {:error, %{conn | mint: mint}, :client_request}

      {:error, mint, reason} when is_atom(reason) ->
        {:error, %{conn | mint: mint}, reason}
    end
  end

  def transfer(conn, pid) do
    case Mint.HTTP1.controlling_process(conn.mint, pid) do
      {:ok, _} ->
        {:ok, conn}

      {:error, error} ->
        Logger.error("API client connection transfer error: " <> Exception.message(error))
        {:error, conn, :transfer_http_process}
    end
  end

  def set_mode(conn, mode) when mode in [:active, :passive] do
    case Mint.HTTP1.set_mode(conn.mint, mode) do
      {:ok, mint} ->
        {:ok, %{conn | mint: mint}}

      {:error, error} ->
        Logger.error("API client set mode error: " <> Exception.message(error))
        {:error, :set_mode}
    end
  end

  def close(%{mint: nil} = conn), do: conn

  def close(conn) do
    {:ok, mint} = Mint.HTTP1.close(conn.mint)
    %{conn | mint: mint}
  end

  def open?(%{mint: nil}), do: false
  def open?(%{mint: mint}), do: Mint.HTTP1.open?(mint)

  defp process_response(%{status: status, data: data}) when status in 100..599 do
    with :error <- Binary.deserialize(data) do
      {:error, :bad_body}
    end
  end

  defp process_response(%{status: status}) do
    Logger.error("API client connection bad status: #{status}")
    {:error, :bad_status}
  end

  defp process_response(_), do: {:error, :bad_response}
end
