defmodule Stepper.Api.Client.Pool do
  @moduledoc """
  Provides Stepper Unix domain socket HTTP1 client pool.  Handles serialization of the request data.
  """

  require Logger

  alias Stepper.Api.Client.Conn
  alias Stepper.Api.{Request, Response}

  @behaviour NimblePool

  @type pool() :: GenServer.server()
  @type result() :: {:ok, Response.t()} | {:error, atom()}

  @spec request(pool(), binary(), binary(), keyword()) :: result()
  def request(pool, method, path, opts), do: request(pool, method, path, nil, opts)

  @spec request(pool(), binary(), binary(), Request.t(), keyword()) :: result()
  def request(pool, method, path, data, opts) do
    pool_timeout = Keyword.get(opts, :pool_timeout, 5000)
    receive_timeout = Keyword.get(opts, :receive_timeout, 15000)

    try do
      NimblePool.checkout!(
        pool,
        :checkout,
        fn from, {state, conn} ->
          with {:ok, conn} <- Conn.connect(conn),
               {:ok, conn, data} <-
                 Conn.request(conn, method, path, data, receive_timeout) do
            {{:ok, data}, transfer_if_open(conn, state, from)}
          else
            {:error, conn, error} ->
              {{:error, error}, transfer_if_open(conn, state, from)}
          end
        end,
        pool_timeout
      )
    catch
      :exit, data ->
        # Provide helpful error messages for known errors
        case data do
          {:timeout, {NimblePool, :checkout, _affected_pids}} ->
            reraise(
              """
              Stepper API client pool was unable to provide a connection within the timeout.  \
              Try creating a bigger pool or seeing if connections are tied up.
              """,
              __STACKTRACE__
            )

          _ ->
            exit(data)
        end
    end
  end

  @impl NimblePool
  def init_worker({socket_path, opts} = state) do
    {:ok, Conn.new(socket_path, self(), opts), state}
  end

  @impl NimblePool
  def handle_checkout(:checkout, _from, %{mint: nil} = conn, pool_state),
    do: {:ok, {:fresh, conn}, conn, pool_state}

  def handle_checkout(:checkout, _from, conn, pool_state) do
    with {:ok, conn} <- Conn.set_mode(conn, :passive) do
      {:ok, {:reuse, conn}, conn, pool_state}
    else
      _ -> {:remove, :closed, pool_state}
    end
  end

  @impl NimblePool
  def handle_checkin(checkin, _from, _old_conn, pool_state) do
    with {:ok, conn} <- checkin,
         {:ok, conn} <- Conn.set_mode(conn, :active) do
      {:ok, %{conn | last_checkin: System.monotonic_time()}, pool_state}
    else
      _ -> {:remove, :closed, pool_state}
    end
  end

  @impl NimblePool
  def handle_update(new_conn, _old_conn, pool_state) do
    {:ok, new_conn, pool_state}
  end

  @impl NimblePool
  # If it is closed, drop it.
  def handle_info(_message, %{mint: nil}), do: {:remove, :closed}

  def handle_info(message, %{mint: mint} = conn) do
    case Mint.HTTP1.stream(mint, message) do
      {:ok, _, _} -> {:ok, conn}
      :unknown -> {:ok, conn}
      {:error, _, _, _} -> {:remove, :closed}
    end
  end

  @impl NimblePool
  # On terminate, effectively close it.
  # This will succeed even if it was already closed or if we don't own it.
  def terminate_worker(_reason, conn, pool_state) do
    Conn.close(conn)
    {:ok, pool_state}
  end

  defp transfer_if_open(conn, state, {pid, _} = from) do
    if Conn.open?(conn) do
      if state == :fresh do
        NimblePool.update(from, conn)

        case Conn.transfer(conn, pid) do
          {:ok, conn} -> {:ok, conn}
          {:error, _, _} -> :closed
        end
      else
        {:ok, conn}
      end
    else
      :closed
    end
  end
end
