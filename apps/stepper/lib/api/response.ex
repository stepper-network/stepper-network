defmodule Stepper.Api.Response do
  @moduledoc """
  The response body for Stepper API applications.
  """

  defmodule InvalidTypeError do
    defexception [:message]
  end

  defguard is_simple_response(term) when term in [:ok, :error]
  defguard is_tuple_response(term) when is_tuple(term) and tuple_size(term) == 2
  defguard is_not_nil(tuple) when not is_nil(elem(tuple, 1))
  defguard is_ok_response(tuple) when elem(tuple, 0) == :ok and is_not_nil(tuple)

  defguard is_error_response(tuple)
           when elem(tuple, 0) == :error and is_not_nil(tuple) and is_atom(elem(tuple, 1))

  defguard is_stepper_result(term)
           when is_simple_response(term) or
                  (is_tuple_response(term) and (is_ok_response(term) or is_error_response(term)))

  @type status() :: :ok | :error
  @type t() :: status() | {:ok, term()} | {:error, atom()}

  @doc """
  Encodes a Stepper API response into the expected format.

  Raises if the response is not formatted properly.
  """
  @spec encode!(t()) :: binary()
  def encode!(response) when is_stepper_result(response) do
    Stepper.Serde.Binary.serialize(response)
  end

  def encode!(bad_response) do
    raise InvalidTypeError,
      message: "Could not encode invalid stepper response #{inspect(bad_response)}"
  end

  @doc """
  Sends a Stepper API response in the expected format (binary Erlang external text format).
  Also sets the headers to the appropriate values.
  """
  @spec send_api_resp(Plug.Conn.t(), Response.t()) :: Plug.Conn.t()
  def send_api_resp(conn, response) do
    conn
    |> Plug.Conn.put_resp_content_type("application/octet-stream")
    |> Plug.Conn.send_resp(200, encode!(response))
  end
end
