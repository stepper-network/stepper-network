defmodule Stepper.Api.Controller do
  defmacro __using__(opts) do
    namespace = Keyword.fetch!(opts, :namespace)

    quote do
      use Phoenix.Controller, unquote(opts)

      import Plug.Conn
      import Stepper.Api.Response

      alias unquote(namespace).Router.Helpers, as: Routes
    end
  end
end
