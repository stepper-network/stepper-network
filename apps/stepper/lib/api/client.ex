defmodule Stepper.Api.Client do
  @moduledoc """
  Client module for Stepper services.

  `Stepper.Client` will provide macros for creating the basic HTTP request
  methods.  For example:

      defmodule MyDaemonClient do
        use Stepper.Client, service: :daemon
      end

  This module takes care of all headers and types expected by Stepper clients.
  """

  alias Stepper.Api.Client.Pool

  defmacro __using__(opts) do
    quote do
      use Supervisor

      require Logger

      @service unquote(opts)[:service] || raise("Stepper client expects :service to be given")
      @poolname String.to_atom("#{__MODULE__}Pool")

      @spec start_link(keyword) :: {:ok, pid}
      def start_link(opts) do
        Supervisor.start_link(__MODULE__, opts, name: __MODULE__)
      end

      @impl true
      def init(opts) do
        socket_path = Stepper.Unix.DomainSocket.path(@service)
        Logger.info("Initializing #{@service} client at socket #{socket_path}")

        children = [
          {NimblePool, worker: {Pool, {socket_path, opts}}, name: @poolname}
        ]

        Supervisor.init(children, strategy: :one_for_one)
      end

      @doc """
      HTTP GET request to Stepper client without body (forbidden with Stepper clients).
      """
      @spec get(binary, keyword) :: Pool.result()
      def get(path, opts \\ []) when is_list(opts), do: Pool.request(@poolname, "GET", path, opts)

      @doc """
      HTTP POST request to Stepper client with body (always required for Stepper clients).
      """
      @spec post(binary, map, keyword) :: Pool.result()
      def post(path, request, opts \\ []),
        do: Pool.request(@poolname, "POST", path, request, opts)

      @doc """
      HTTP DELETE request to Stepper client with body.
      """
      @spec delete(binary, map, keyword) :: Pool.result()
      def delete(path, request, opts \\ []),
        do: Pool.request(@poolname, "DELETE", path, request, opts)
    end
  end
end
