defmodule Stepper.Api.Request do
  @moduledoc """
  The request body for Stepper API applications.
  """

  defmodule RequestTypeError do
    defexception [:message]
  end

  @type t() :: nil | map() | struct()

  @doc """
  Encodes a Stepper API response into the expected format.

  Raises if the response is not formatted properly.
  """
  @spec encode!(t()) :: binary()
  def encode!(nil), do: encode!(%{})
  def encode!(request) when is_struct(request), do: encode!(Map.from_struct(request))

  def encode!(request) when is_map(request) do
    Stepper.Serde.Binary.serialize(request)
  end

  def encode!(bad_request) do
    raise RequestTypeError,
          "invalid stepper request type, must be a map or struct: #{inspect(bad_request)}"
  end
end
