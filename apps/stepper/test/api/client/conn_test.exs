defmodule Stepper.Api.Client.ConnTest do
  use ExUnit.Case, async: false
  use Plug.Test

  require Stepper.Api.Response

  alias Stepper.Api.Client.Conn
  alias Stepper.ApiServer

  @socket_name "api_client_conntest.socket"
  @request_timeout 5_000
  @mint_exception %Mint.TransportError{reason: :timeout}

  setup_all do
    {:ok, _} = ApiServer.start(@socket_name)

    {:ok, socket_path: ApiServer.socket_path(@socket_name)}
  end

  describe "new/3" do
    test "creates expected object", %{socket_path: socket_path} do
      parent = parent()
      conn = Conn.new(socket_path, parent, test: "test")

      assert match?(
               %{
                 host: {:local, ^socket_path},
                 mint: nil,
                 opts: [test: "test"],
                 parent: ^parent
               },
               conn
             )

      assert conn.last_checkin < System.monotonic_time()
    end
  end

  describe "connect/1" do
    setup [:setup_conn]

    test "successful", %{conn: conn} do
      # mint is null before connection, set after
      refute conn.mint
      assert {:ok, conn} = Conn.connect(conn)
      assert conn.mint

      # if already connected, calling connect/1 should just return the connection unmodified
      assert match?({:ok, ^conn}, Conn.connect(conn))
    end

    test "unsuccessful", %{conn: conn} do
      # we can test an unsuccessful connection by using a socket file that does not exist
      conn = %{conn | host: {:local, ApiServer.socket_path("missing.socket")}}

      refute File.exists?(elem(conn.host, 1))
      refute conn.mint
      assert {:error, ^conn, :client_connect} = Conn.connect(conn)
      refute conn.mint
    end
  end

  describe "before client connect" do
    setup [:setup_conn]

    test "request/5 failure", %{conn: conn} do
      assert {:error, _, :not_connected} =
               Conn.request(conn, "POST", "/ok", %{}, @request_timeout)
    end

    test "open?/1", %{conn: conn} do
      refute Conn.open?(conn)
    end

    test "close/1", %{conn: conn} do
      assert ^conn = Conn.close(conn)
    end
  end

  describe "request/5 after connection" do
    setup [:setup_conn, :connect_conn]

    test "successful :ok response", %{conn: conn} do
      body = %{a: "A", b: "B"}
      assert {:ok, _, {:ok, ^body}} = Conn.request(conn, "POST", "/ok", body, @request_timeout)
    end

    test "successful struct :ok response", %{conn: conn} do
      body = Stepper.Unix.Auth.new("test", "testpassword")
      body_map = Map.from_struct(body)

      assert {:ok, _, {:ok, ^body_map}} =
               Conn.request(conn, "POST", "/ok", body, @request_timeout)
    end

    test "successful :error response", %{conn: conn} do
      assert {:ok, _, {:error, :test}} =
               Conn.request(conn, "GET", "/error", nil, @request_timeout)
    end

    test "failure client request", %{conn: conn} do
      Mimic.stub(Mint.HTTP1, :request, fn mint, _method, _path, _headers, _body ->
        {:error, mint, @mint_exception}
      end)

      assert {:error, _, :client_request} =
               Conn.request(conn, "GET", "/error", nil, @request_timeout)
    end

    test "failure client receive", %{conn: conn} do
      Mimic.stub(Mint.HTTP1, :recv, fn mint, _byte_count, _timeout ->
        {:error, mint, @mint_exception, []}
      end)

      assert {:error, _, :http_receive} =
               Conn.request(conn, "GET", "/error", nil, @request_timeout)
    end

    test "failure to process response bad status", %{conn: conn} do
      Mimic.stub(Stepper.Client, :receive_response, fn mint, _ref, _timeout ->
        {:ok, mint, %{status: 0}}
      end)

      # stub will override the result of this request, so actual request doesn't matter
      assert {:error, _, :bad_status} = Conn.request(conn, "POST", "/ok", nil, @request_timeout)
    end

    test "failure to process response bad response", %{conn: conn} do
      Mimic.stub(Stepper.Client, :receive_response, fn mint, _ref, _timeout ->
        {:ok, mint, %{}}
      end)

      # stub will override the result of this request, so actual request doesn't matter
      assert {:error, _, :bad_response} = Conn.request(conn, "POST", "/ok", nil, @request_timeout)
    end
  end

  describe "transfer/2" do
    setup [:setup_conn, :connect_conn]

    test "successful", %{conn: conn} do
      assert {:ok, ^conn} = Conn.transfer(conn, self())
    end

    test "failure", %{conn: conn} do
      Mimic.stub(Mint.HTTP1, :controlling_process, fn _mint, _pid -> {:error, @mint_exception} end)

      assert {:error, ^conn, :transfer_http_process} = Conn.transfer(conn, self())
    end
  end

  describe "set_mode/2" do
    setup [:setup_conn, :connect_conn]

    test "successful", %{conn: conn} do
      # not ideal as we are peering into a library object so fix if needed
      assert {:ok, %{mint: %{mode: :active}}} = Conn.set_mode(conn, :active)
      assert {:ok, %{mint: %{mode: :passive}}} = Conn.set_mode(conn, :passive)
    end

    test "failure", %{conn: conn} do
      Mimic.stub(Mint.HTTP1, :set_mode, fn _mint, _mode -> {:error, @mint_exception} end)

      assert {:error, :set_mode} = Conn.set_mode(conn, :active)
    end
  end

  describe "close/1" do
    setup [:setup_conn, :connect_conn]

    test "successful", %{conn: conn} do
      assert Conn.open?(conn)
      conn = Conn.close(conn)
      refute Conn.open?(conn)
    end
  end

  defp setup_conn(%{socket_path: socket_path}),
    do: {:ok, conn: Conn.new(socket_path, parent(), [])}

  defp connect_conn(%{conn: conn}) do
    {:ok, conn} = Conn.connect(conn)
    {:ok, %{conn: conn}}
  end

  defp parent(), do: self()
end
