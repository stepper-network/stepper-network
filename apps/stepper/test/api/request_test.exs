defmodule Stepper.Api.RequestTest do
  use ExUnit.Case, async: true

  alias Stepper.Api.Request

  @test_map %{test1: "test1", test2: "test2"}

  defmodule TestStruct do
    defstruct test1: "test1", test2: "test2"
  end

  @encoded_empty_map "\x83t\0\0\0\0"
  @encoded_map "\x83t\0\0\0\x02w\x05test1m\0\0\0\x05test1w\x05test2m\0\0\0\x05test2"

  describe "encode!/1" do
    test "successful" do
      assert Request.encode!(nil) == @encoded_empty_map
      assert Request.encode!(%{}) == @encoded_empty_map
      assert Request.encode!(@test_map) == @encoded_map
      assert Request.encode!(%TestStruct{}) == @encoded_map
    end

    test "failure" do
      assert_raise(Request.RequestTypeError, fn -> Request.encode!(:test) end)
      assert_raise(Request.RequestTypeError, fn -> Request.encode!(0) end)
      assert_raise(Request.RequestTypeError, fn -> Request.encode!(0.1) end)
      assert_raise(Request.RequestTypeError, fn -> Request.encode!("test") end)
    end
  end
end
