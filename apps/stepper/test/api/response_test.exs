defmodule Stepper.Api.ResponseTest do
  use ExUnit.Case, async: true
  use Plug.Test

  require Stepper.Api.Response

  alias Stepper.Api.Response

  describe "helper guard" do
    test "is_simple_response" do
      assert Response.is_simple_response(:ok)
      assert Response.is_simple_response(:error)

      refute Response.is_simple_response(:nodata)
      refute Response.is_simple_response("asdf")
      refute Response.is_simple_response(nil)
    end

    test "is_tuple_response" do
      assert Response.is_tuple_response({:test, :test})
      assert Response.is_tuple_response({nil, nil})

      refute Response.is_tuple_response({})
      refute Response.is_tuple_response({:test})
      refute Response.is_tuple_response({:test, :test, :test})
    end

    test "is_not_nil" do
      assert Response.is_not_nil({:test, :test})
      assert Response.is_not_nil({:test, :test, :test})

      refute Response.is_not_nil({:test, nil})
      refute Response.is_not_nil({nil, nil})
    end

    test "is_ok_response" do
      assert Response.is_ok_response({:ok, :test})
      assert Response.is_ok_response({:ok, :nodata})
      assert Response.is_ok_response({:ok, :test, :test})

      refute Response.is_ok_response({:ok, nil})
    end

    test "is_error_response" do
      assert Response.is_error_response({:error, :test})
      assert Response.is_error_response({:error, :nodata})

      refute Response.is_error_response({:error, "string"})
      refute Response.is_error_response({:error, nil})
    end

    test "is_stepper_result" do
      assert Response.is_stepper_result(:ok)
      assert Response.is_stepper_result(:error)
      assert Response.is_stepper_result({:ok, :test})
      assert Response.is_stepper_result({:error, :test})

      refute Response.is_stepper_result({})
      refute Response.is_stepper_result({:test})
      refute Response.is_stepper_result({:test, :test})
      refute Response.is_stepper_result({nil, nil})
      refute Response.is_stepper_result({:test})
      refute Response.is_stepper_result({:ok, :test, :test})
      refute Response.is_stepper_result({:error, :test, :test})
    end
  end

  describe "encode!/1" do
    test "valid Stepper response" do
      assert Response.encode!(:ok) == "\x83w\x02ok"
      assert Response.encode!(:error) == "\x83w\x05error"
      assert Response.encode!({:ok, :nodata}) == "\x83h\x02w\x02okw\x06nodata"
      assert Response.encode!({:ok, %{a: "b"}}) == "\x83h\x02w\x02okt\0\0\0\x01w\x01am\0\0\0\x01b"
      assert Response.encode!({:error, :test}) == "\x83h\x02w\x05errorw\x04test"
    end

    test "invalid Stepper response" do
      assert_raise(Response.InvalidTypeError, fn -> Response.encode!(:test) end)
      assert_raise(Response.InvalidTypeError, fn -> Response.encode!(nil) end)
      assert_raise(Response.InvalidTypeError, fn -> Response.encode!({:ok, nil}) end)
      assert_raise(Response.InvalidTypeError, fn -> Response.encode!({:error, nil}) end)
    end
  end

  describe "send_api_resp/2" do
    setup [:setup_conn]

    test "valid Stepper response", %{conn: conn} do
      {status, headers, body} = conn |> Response.send_api_resp({:ok, :nodata}) |> sent_resp()

      assert status == 200
      assert Enum.member?(headers, {"content-type", "application/octet-stream; charset=utf-8"})
      assert body == "\x83h\x02w\x02okw\x06nodata"
    end

    test "invalid Stepper response", %{conn: conn} do
      assert_raise(Response.InvalidTypeError, fn -> Response.send_api_resp(conn, :test) end)
    end
  end

  defp setup_conn(_) do
    {:ok, %{conn: conn(:get, "/")}}
  end
end
