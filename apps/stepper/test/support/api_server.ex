defmodule Stepper.ApiServer do
  @moduledoc false
  alias Stepper.Unix.DomainSocket

  def socket_path(name), do: Path.join([System.tmp_dir!(), name])

  def start(name) do
    socket_path = socket_path(name)

    children = [
      Plug.Adapters.Cowboy.child_spec(
        scheme: :http,
        plug: Stepper.ApiServer.PlugRouter,
        options: [
          ip: {:local, socket_path},
          port: 0,
          otp_app: :stepper,
          protocol_options: [
            idle_timeout: 3_000,
            request_timeout: 10_000
          ]
        ]
      )
    ]

    DomainSocket.prepare!(socket_path)
    Supervisor.start_link(children, strategy: :one_for_one)
  end
end

# Super simple API router for testing the API client.
# Expected to return similar responses to the API.  Bad request bodies will raise an exception.
defmodule Stepper.ApiServer.PlugRouter do
  @moduledoc false
  use Plug.Router

  import Stepper.Api.Response
  import Stepper.Serde.Binary

  plug(:match)
  plug(:dispatch)

  post "/ok" do
    {conn, body} = process_body(conn)

    conn
    |> send_api_resp({:ok, body})
    |> halt()
  end

  get "/error" do
    conn
    |> send_api_resp({:error, :test})
    |> halt()
  end

  defp process_body(conn) do
    {:ok, body, conn} = Plug.Conn.read_body(conn)
    {:ok, body} = deserialize(body)
    {conn, body}
  end
end
