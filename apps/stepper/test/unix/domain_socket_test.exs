defmodule Stepper.Unix.DomainSocketTest do
  @moduledoc """
  Tests the Stepper domain socket helpers.

  IMPORTANT: Cannot run async due to how tests are structured.
  """
  use ExUnit.Case

  import Bitwise

  alias Stepper.Unix.DomainSocket
  alias Stepper.Unix.FileSystem

  @socket_name "domainsockettest.socket"

  describe "path/1" do
    setup [:setup_run_dir]

    test "service paths" do
      assert DomainSocket.path(:daemon) == test_socket("daemon.socket")
      assert DomainSocket.path(:state) == test_socket("state.socket")
    end

    test "full path" do
      assert DomainSocket.path(@socket_name) == test_socket(@socket_name)
    end

    test "bad service atom" do
      assert_raise(DomainSocket.SocketError, fn -> DomainSocket.path(:test) end)
    end
  end

  describe "prepare!/1" do
    setup [:setup_run_dir]

    test "existing file" do
      test_socket = test_socket(@socket_name)
      File.touch!(test_socket)
      on_exit(fn -> File.rm(test_socket) end)

      assert File.exists?(test_socket)
      DomainSocket.prepare!(test_socket)
      refute File.exists?(test_socket)
    end

    test "non-existing file" do
      test_socket = test_socket(@socket_name)

      refute File.exists?(test_socket)
      DomainSocket.prepare!(test_socket)
      refute File.exists?(test_socket)
    end

    test "bad service atom" do
      assert_raise(DomainSocket.SocketError, fn -> DomainSocket.prepare!(:test) end)
    end
  end

  describe "update_permissions/1" do
    test "successfully" do
      test_socket = test_socket(@socket_name)
      File.touch!(test_socket)
      on_exit(fn -> File.rm(test_socket) end)

      assert (File.stat!(test_socket).mode &&& 0o666) == 0o644
      DomainSocket.update_permissions!(test_socket)
      assert (File.stat!(test_socket).mode &&& 0o666) == 0o660
    end

    test "unsuccessfully" do
      test_socket = test_socket(@socket_name)

      refute File.exists?(test_socket)

      assert_raise(DomainSocket.SocketError, fn ->
        DomainSocket.update_permissions!(test_socket)
      end)
    end
  end

  defp setup_run_dir(_) do
    Mimic.stub(Stepper.Unix.FileSystem, :get_run_dir, fn -> System.tmp_dir!() end)
    :ok
  end

  defp test_socket(name), do: Path.join(FileSystem.get_run_dir(), name)
end
