defmodule Stepper.ClientTest do
  use ExUnit.Case, async: false

  alias Stepper.Client

  @request_timeout 5_000
  @mint_exception %Mint.TransportError{reason: :timeout}

  describe "receive_response/3" do
    test "successful" do
      ref = 0

      entries = [
        {:status, ref, 200},
        {:headers, ref, [{"test", "header"}]},
        {:data, ref, :test},
        {:done, ref}
      ]

      Mimic.stub(Mint.HTTP1, :recv, fn _mint, _byte_count, _timeout -> {:ok, nil, entries} end)

      assert {:ok, nil, %{data: :test, headers: [{"test", "header"}], status: 200}} =
               Client.receive_response(nil, ref, @request_timeout)
    end

    test "receive error" do
      ref = 10

      Mimic.stub(Mint.HTTP1, :recv, fn _mint, _byte_count, _timeout ->
        {:error, nil, @mint_exception, []}
      end)

      assert {:error, nil, :http_receive} = Client.receive_response(nil, ref, @request_timeout)
    end

    test "receive entry error" do
      ref = 20

      Mimic.stub(Mint.HTTP1, :recv, fn _mint, _byte_count, _timeout ->
        {:ok, nil, [{:error, ref, @mint_exception}]}
      end)

      assert {:error, nil, :response_entry} = Client.receive_response(nil, ref, @request_timeout)
    end
  end
end
