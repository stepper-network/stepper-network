defmodule Stepper.MixProject do
  use Mix.Project

  def project do
    [
      app: :stepper,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      deps: deps()
    ]
  end

  def application do
    []
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:excoveralls, "~> 0.10", only: :test},
      {:mimic, "~> 1.7", only: :test},
      {:mint, "~> 1.4"},
      {:nimble_pool, "~> 0.2"},
      {:plug, "~> 1.14"},
      {:plug_cowboy, "~> 2.0", only: :test}
    ]
  end
end
