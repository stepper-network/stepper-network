// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
// import "./user_socket.js"

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "../vendor/some-package.js"
//
// Alternatively, you can `npm install some-package --prefix assets` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

import { DataTable } from "simple-datatables"
// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html"
// Establish Phoenix Socket and LiveView configuration.
import { Socket } from "phoenix"
import { LiveSocket } from "phoenix_live_view"
import topbar from "../vendor/topbar"

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let hooks = {
    // Allows attaching the simple-datatables JS librasary to a table.  Table can be configured
    // through `<th>` tags.  See https://fiduswriter.github.io/simple-datatables/documentation/columns.
    SortableTable: {
        mounted() {
            new DataTable(this.el, {
                footer: false,
                perPageSelect: [10, 15, 20, ["All", -1]]
            })
        }
    },
    // The dark theme logic is handled by Tailwind annotations.  We use Javascript hooks to store the
    // user selection in localStorage for some persistence.  LiveView has no idea what the theme is 
    // since everything is handled by CSS.  This must hook into a checkbox input.
    StoreThemeSetting: {
        mounted() {
            // logic adapted from Tailwind documentation for dark themes
            if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
                document.documentElement.classList.add('dark');
                this.el.checked = true;
            } else {
                document.documentElement.classList.remove('dark');
                this.el.checked = false;
            }

            // `this.el` will be a form, so `this.el.username` will be the field named "username".
            // When this field is changed, store its value.
            this.el.addEventListener("change", e => {
                if (e.currentTarget.checked) {
                    document.documentElement.classList.add('dark');
                    localStorage.setItem("theme", "dark");
                } else {
                    document.documentElement.classList.remove('dark');
                    localStorage.removeItem("theme");
                }
            })
        }
    }
}
let liveSocket = new LiveSocket("/live", Socket, { params: { _csrf_token: csrfToken }, hooks: hooks })

// Show progress bar on live navigation and form submits
topbar.config({ barColors: { 0: "#5e81ac" }, shadowColor: "rgba(0, 0, 0, .3)" })
window.addEventListener("phx:page-loading-start", info => topbar.delayedShow(100))
window.addEventListener("phx:page-loading-stop", info => topbar.hide())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket
