// See the Tailwind configuration guide for advanced usage
// https://tailwindcss.com/docs/configuration

const plugin = require("tailwindcss/plugin");
const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors')

module.exports = {
    content: [
        "./js/**/*.js",
        "../lib/*_web.ex",
        "../lib/*_web/**/*.*ex"
    ],
    darkMode: 'class',
    theme: {
        extend: {
            colors: {
                transparent: 'transparent',
                current: 'currentColor',
                core: {
                    0: colors.stone[300],
                    1: colors.stone[400],
                    2: colors.stone[500],
                    3: colors.stone[600],
                    4: colors.stone[800],
                    5: colors.stone[900],
                },
                hl: {
                    0: '#8756D6',
                    1: '#57378A',
                    2: '#472D70',
                },
                input: {
                    0: '#6DBFAD',
                    1: '#498074',
                    2: '#1D332E',
                },
                button: {
                    0: '#729CA8',
                    1: '#638691',
                    2: '#374C52',
                },
            },
            fontFamily: {
                inter: ['Inter var', ...defaultTheme.fontFamily.sans]
            },
            scale: {
                '98': '0.98',
                '99': '0.99'
            },
        },
    },
    plugins: [
        require("@tailwindcss/forms"),
        plugin(({ addVariant }) => addVariant("phx-no-feedback", [".phx-no-feedback&", ".phx-no-feedback &"])),
        plugin(({ addVariant }) => addVariant("phx-click-loading", [".phx-click-loading&", ".phx-click-loading &"])),
        plugin(({ addVariant }) => addVariant("phx-submit-loading", [".phx-submit-loading&", ".phx-submit-loading &"])),
        plugin(({ addVariant }) => addVariant("phx-change-loading", [".phx-change-loading&", ".phx-change-loading &"]))
    ]
}
