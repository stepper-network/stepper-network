defmodule StepperGuiWeb.SidebarComponent do
  use StepperGuiWeb, :live_component

  alias StepperGuiWeb.Icons

  def render(assigns) do
    ~H"""
    <div id="sidebar" class="flex flex-col items-center sticky top-0 w-60 h-screen overflow-hidden text-core-4 dark:text-core-1 bg-core-1 dark:bg-core-5">
      <div id="sidebar-info" class="flex flex-col items-left">
        <.logo />
        <.system username={@username} hostname={@hostname} />
      </div>
      <.navlist id="sidebar-navigate">
        <:item name="Dashboard" navigate={~p"/dashboard"}><Icons.dashboard class="w-6 h-6" /></:item>
        <:item name="Sessions" navigate={~p"/sessions"}><Icons.sessions class="w-6 h-6" /></:item>
        <:item name="Users" navigate={~p"/users"}><Icons.sessions class="w-6 h-6" /></:item>
      </.navlist>
        <.link id="sidebar-bottom" class="flex items-center w-full h-12 px-3 mt-auto border-t border-inherit" href={~p"/users/log_out"} method="delete">
          <Icons.log_out class="w-6 h-6" />
          <span class="ml-2 text-md font-bold">Log Out</span>
        </.link>
    </div>
    """
  end

  def mount(assigns) do
    assigns =
      assign_new(assigns, :hostname, fn ->
        {:ok, hostname} = :inet.gethostname()
        hostname
      end)

    {:ok, assigns}
  end

  defp logo(assigns) do
    ~H|<h2 class="ml text-2xl font-bold">Stepper Network</h2>|
  end

  attr :username, :string, required: true
  attr :hostname, :string, required: true

  defp system(assigns) do
    ~H"""
    <div class="flex flex-row items-center">
      <div class="grow shrink ml text-sm font-light">
        <p>user: <%= @username || "?" %></p>
        <p>host: <%= @hostname %></p>
      </div>
      <div class="flex-none" title="Dark mode selector">
        <input type="checkbox" id="sidebar-dark-mode-checkbox" phx-hook="StoreThemeSetting" class="hidden pointer-none">
        <label for="sidebar-dark-mode-checkbox" id="sidebar-dark-mode-icon" class="cursor-pointer">
          <Icons.dark_mode class="w-7 h-7 transition duration-300 ease-in-out hover:transition-all hover:scale-[1.15]" />
        </label>
      </div>
    </div>
    """
  end

  slot :item, required: true do
    attr :name, :string, required: true
    attr :navigate, :string, required: true
  end

  defp navlist(assigns) do
    ~H"""
    <nav class="w-full px-2 mt-3 border-t">
      <div class="flex flex-col items-center w-full">
        <%= for item <- @item do %>
          <.navitem name={item[:name]} href={item[:navigate]}>
            <%= render_slot(item) %>
          </.navitem>
        <% end %>
      </div>
    </nav>
    """
  end

  attr :name, :string, required: true
  attr :rest, :global, include: ~w(navigate href)
  slot :inner_block, required: true

  defp navitem(assigns) do
    IO.inspect(assigns)

    ~H"""
    <.link
      class="flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-sky hover:text-slate-dark"
      {@rest}
    >
      <%= render_slot(@inner_block) %>
      <span class="ml-2 text-md font-bold"><%= @name %></span>
    </.link>
    """
  end
end
