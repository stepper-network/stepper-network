defmodule StepperGuiWeb.Layouts do
  use StepperGuiWeb, :html

  embed_templates("layouts/*")
end
