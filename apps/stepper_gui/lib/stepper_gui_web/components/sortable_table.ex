defmodule StepperGuiWeb.SortableTableComponent do
  @moduledoc ~S"""
  Renders a table with Stepper GUI styling.  It uses the simple-datatables JS library to add
  sorting functionality.  Note that the column attributes for the JS library are passed through
  the `:col` slots.

  ## Examples

      <.live_component module={SortableTable} id="system-users" rows={@users}>
        <:col :let={user} label="id"><%= user.id %></:col>
        <:col :let={user} label="username" sortable="false" searchable="false"><%= user.name %></:col>
      </.live_component>
  """

  use StepperGuiWeb, :live_component

  attr :id, :string, required: true
  attr :heading, :string, required: true
  attr :rows, :list, required: true

  slot :col, required: true do
    attr :label, :string
    attr :sortable, :boolean
    attr :serachable, :boolean
  end

  slot :action

  @doc """
  Much of the CSS configuration happens in the `../assets/css/app.css` file due to how the
  simple-datatables library works.  The JS will basically invalidate whatever we configure
  here for within the table tag.
  """
  def render(assigns) do
    ~H"""
    <div id={@id} class="p-4 bg-core-1 dark:bg-core-5 overflow-x-auto">
      <h1 class="text-2xl font-bold mx-2 py-2 border-b"><%= @heading %></h1>
      <table id={"#{@id}-table"} phx-hook="SortableTable">
        <thead>
          <tr>
            <th
              :for={col <- @col}
              data-sortable={to_string(Map.get(col, :sortable, true))}
              data-searchable={to_string(Map.get(col, :searchable, true))}
            >
              <%= col[:label] %>
            </th>
            <th :if={@action != []}><span class="sr-only"><%= gettext("Actions") %></span></th>
          </tr>
        </thead>
        <tbody>
          <tr :for={row <- @rows}>
            <td :for={{col, i} <- Enum.with_index(@col)}>
              <div class="block px-1 py-1 border-1">
                <span class="relative">
                  <%= render_slot(col, row) %>
                </span>
              </div>
            </td>
            <td :if={@action != []} class="p-0 w-14">
              <div class="relative whitespace-nowrap py-4 text-right text-sm font-medium">
                <span :for={action <- @action} class="relative ml-4 font-semibold leading-6 text-zinc-900 hover:text-zinc-700">
                  <%= render_slot(action, row) %>
                </span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    """
  end
end
