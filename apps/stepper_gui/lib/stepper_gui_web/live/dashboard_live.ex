defmodule StepperGuiWeb.DashboardLive do
  use StepperGuiWeb, :live_view

  def render(assigns) do
    ~H"""
    <section class="row">
    <h1>Dashboard</h1>
    </section>
    """
  end

  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:page_title, "System Dashboard")}
  end
end
