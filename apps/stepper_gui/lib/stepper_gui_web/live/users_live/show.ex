defmodule StepperGuiWeb.UserLive.Show do
  use StepperGuiWeb, :live_view

  alias StepperGui.StateClient
  alias Phoenix.LiveView.Socket

  def render(assigns) do
    ~H"""
    <h2>Show User</h2>
    <ul>
      <li><b>Username:</b> <%= @user.username %></li>
      <li><b>Email:</b> <%= @user.email %></li>
      <li><b>Phone:</b> <%= @user.phone_number %></li>
    </ul>
    <span patch={~p"/users/#{@user.id}/edit"}>Edit</span>
    <span patch={~p"/users"}>Back</span>
    """
  end
end
