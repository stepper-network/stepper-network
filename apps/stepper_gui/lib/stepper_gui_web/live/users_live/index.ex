defmodule StepperGuiWeb.UsersLive.Index do
  use StepperGuiWeb, :live_view

  alias StepperGui.StateClient
  alias StepperGuiWeb.SortableTableComponent

  def render(assigns) do
    ~H"""
    <.live_component module={SortableTableComponent} id="system-users" heading="System Users" rows={@users}>
      <:col :let={user} label="Id" sort="asc"><%= user.id %></:col>
      <:col :let={user} label="Username"><%= user.name %></:col>
      <:col :let={user} label="Group Id"><%= user.group_id %></:col>
      <:col :let={user} label="Home Directory" sortable="false"><%= user.home_dir %></:col>
      <:col :let={user} label="Login Shell" sortable="false"><%= user.login_shell %></:col>
    </.live_component>
    """
  end

  def mount(_params, _session, socket) do
    {:ok, {:ok, {users, _groups}}} = StateClient.get("/system/accounts")
    users = Enum.sort(users, &(&1.id <= &2.id))
    {:ok, assign(socket, page_title: "System Users", users: users)}
  end
end
