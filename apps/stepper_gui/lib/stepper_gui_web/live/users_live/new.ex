defmodule StepperGuiWeb.UsersLive.New do
  use StepperGuiWeb, :live_view

  def render(assigns) do
    ~H"""
    <h1>New System user</h1>
    """
  end

  def handle_event("save", %{"user" => _user_params}, socket) do
    {:noreply, socket}
  end
end
