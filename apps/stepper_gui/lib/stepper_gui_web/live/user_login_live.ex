defmodule StepperGuiWeb.UserLoginLive do
  use StepperGuiWeb, :live_view

  def render(assigns) do
    ~H"""
    <div class="mx-auto my-12 w-96 p-4 text-white-dark bg-slate-light rounded-lg">
      <.header class="text-left">
        Sign in to this Stepper box
        <:subtitle>
          Must be a Stepper user on this box
        </:subtitle>
      </.header>

      <.simple_form
        :let={f}
        id="login_form"
        for={:user}
        action={~p"/users/log_in"}
        as={:user}
        phx-update="ignore"
      >
        <.input field={{f, :username}} type="text" label="Username" required />
        <.input field={{f, :password}} type="password" label="Password" required />

        <:actions :let={f}>
          <.input field={{f, :remember_me}} type="checkbox" label="Keep me logged in" />
        </:actions>
        <:actions>
          <.button phx-disable-with="Signing in..." class="w-full">
            Sign in <span aria-hidden="true">→</span>
          </.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  def mount(_params, _session, socket) do
    email = live_flash(socket.assigns.flash, :email)
    {:ok, assign(socket, email: email, page_title: "Log In"), temporary_assigns: [email: nil]}
  end
end
