defmodule StepperGuiWeb.SessionsLive do
  use StepperGuiWeb, :live_view

  def render(assigns) do
    ~H"""
    <section class="row">
    <h1>Sessions Dashboard</h1>
    </section>
    """
  end

  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:page_title, "User Sessions")}
  end
end
