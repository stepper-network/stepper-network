defmodule StepperGuiWeb.PageController do
  use StepperGuiWeb, :controller

  def redirect_to_dashboard(conn, _params) do
    redirect(conn, to: "/dashboard")
  end
end
