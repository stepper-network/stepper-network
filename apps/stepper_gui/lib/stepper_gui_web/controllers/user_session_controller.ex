defmodule StepperGuiWeb.UserSessionController do
  use StepperGuiWeb, :controller

  require Logger

  alias Stepper.Unix.{Auth, Group, User}
  alias StepperGui.StateClient
  alias StepperGuiWeb.UserAuth

  def create(conn, %{"user" => user}) do
    {auth, remember_me} = extract_user_params(user)

    with {:ok, {:ok, {%User{} = user, [%Group{}] = groups}}} <-
           StateClient.post("/system/auth", auth) do
      conn
      |> put_flash(:info, "Welcome back!")
      |> UserAuth.log_in_user(user, remember_me)
    else
      {:error, reason} ->
        Logger.error("Could not authenticate user #{auth.username}, #{reason}")

        conn
        |> put_flash(:error, "Authentication service down")
        |> redirect(to: ~p"/users/log_in")

      {:ok, {:error, reason}} ->
        Logger.info("Authentication failure for #{auth.username}, #{reason}")

        conn
        |> put_flash(:error, "Invalid username or password")
        |> put_flash(:username, String.slice(auth.username, 0, 160))
        |> redirect(to: ~p"/users/log_in")
    end
  end

  defp extract_user_params(user_params) do
    %{"username" => username, "password" => password, "remember_me" => remember_me} = user_params
    auth = Auth.new(username, password)

    remember_me =
      if remember_me == "true" do
        true
      else
        false
      end

    {auth, remember_me}
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> UserAuth.log_out_user()
  end
end
