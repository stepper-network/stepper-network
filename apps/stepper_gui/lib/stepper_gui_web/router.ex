defmodule StepperGuiWeb.Router do
  use StepperGuiWeb, :router

  import StepperGuiWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {StepperGuiWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  scope "/", StepperGuiWeb do
    pipe_through :browser

    get "/", PageController, :redirect_to_dashboard
  end

  # Only enable the LiveDashboard in dev/test environments.  Note that the URL is non-standard.
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/live-dashboard", metrics: StepperGuiWeb.Telemetry
    end
  end

  scope "/", StepperGuiWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [{StepperGuiWeb.UserAuth, :redirect_if_user_is_authenticated}] do
      live "/users/log_in", UserLoginLive, :new
    end

    post "/users/log_in", UserSessionController, :create
  end

  scope "/", StepperGuiWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [{StepperGuiWeb.UserAuth, :ensure_authenticated}] do
      live "/dashboard", DashboardLive
      live "/sessions", SessionsLive
      live "/users", UsersLive.Index
      live "/users/new", UsersLive.New
      live "/users/:id", UsersLive.Show
      live "/users/:id/edit", UsersLive.Edit, :edit
    end
  end

  scope "/", StepperGuiWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
  end
end
