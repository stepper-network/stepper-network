defmodule StepperGui.SessionCache do
  @moduledoc """
  Cache of sessions for the GUI.

  The cache fetches sessions stored in by the Stepper state service.  Even if this fails, the cache
  should be in a usable state restricted to new sessions only.

  Initialization of the cache is handled asyncronously after GenServer initialization.

  The cache features a cleaner to remove sessions that have reached either the idle timeout or
  session timeout.  It also retries all put/delete storage failures.

  ## Initialization States
    * `:uninitialized` - The ETS tables have not been set up
    * `:unfetched` - The session data has not been fetched from the state service
    * `:ok` - Initialization has completed
  """

  use GenServer

  require Logger

  alias Stepper.Session
  alias StepperGui.StateClient

  @enforce_keys [:status, :delete_fails, :put_fails]
  defstruct @enforce_keys

  def start_link(opts \\ []) do
    state = %__MODULE__{status: :uninitialized, put_fails: [], delete_fails: []}
    opts = Keyword.put(opts, :name, __MODULE__)
    GenServer.start_link(__MODULE__, state, opts)
  end

  def generate_new(session_data, tries \\ 3) do
    idle_timeout = get_config!(:idle_timeout_ms)
    max_age = get_config!(:max_age_ms)
    generate_new(session_data, idle_timeout, max_age, tries)
  end

  def generate_new(session_data, idle_timeout, max_age, tries) when tries > 0 do
    apply_to_cache(fn table ->
      session = Session.build(session_data, max_age)

      if :ets.insert_new(table, {session.token, session, Session.now(), idle_timeout}) do
        Process.send(__MODULE__, {:put_in_store, session}, [])
        session.token
      else
        generate_new(session_data, idle_timeout, max_age, tries - 1)
      end
    end)
  end

  def generate_new(_session_data, _idle_timeout, _max_age, _tries),
    do: raise("Could not generate new session")

  @doc """
  Gets the session from the cache by the token.  Always succeeds.
  """
  @spec get(binary) :: nil | Session.t()
  def get(token), do: apply_to_cache(&get_from_cache(&1, token))

  def get_user(token), do: (session = get(token)) && Map.get(session.data, :user)

  defp get_from_cache(table, token) do
    case :ets.lookup(table, token) do
      [{^token, session, last_touched, idle_timeout}] ->
        now = Session.now()

        unless Session.expired?(session) or
                 last_touched + idle_timeout < now do
          :ets.update_element(table, token, {3, now})
          session
        else
          :ets.delete(table, token)
          Process.send(__MODULE__, {:delete_from_store, token}, [])
          nil
        end

      [] ->
        nil
    end
  end

  @doc """
  Deletes the session from the cache and store.  Store delete is asyncronous.
  """
  @spec delete(binary) :: :ok | {:error, binary}
  def delete(token) do
    apply_to_cache(fn table ->
      :ets.delete(table, token)
      Process.send(__MODULE__, {:delete_from_store, token}, [])
      :ok
    end)
  end

  @impl true
  def init(%__MODULE__{} = state) do
    cleanup_ms = get_config!(:cleanup_ms)
    Process.send_after(self(), :cleanup, cleanup_ms)
    {:ok, state, {:continue, :initialize}}
  end

  @impl true
  def handle_continue(:initialize, %__MODULE__{status: :uninitialized} = state) do
    try do
      :ets.new(:session_cache_table, [:named_table])
    rescue
      _e in ArgumentError -> Logger.warn("Session cache top-level table already created")
    end

    status =
      retry_ets_operation(fn ->
        session_table = :ets.new(generate_table_name(), [:named_table, :public])

        if :ets.insert_new(
             :session_cache_table,
             {:current, session_table, :unfetched, Session.now()}
           ) do
          :unfetched
        else
          get_status()
        end
      end)

    {:noreply, %{state | status: status}, {:continue, :initialize}}
  end

  @impl true
  def handle_continue(:initialize, %__MODULE__{status: :unfetched} = state) do
    retry_ms = get_config!(:retry_fetch_ms)
    idle_timeout = get_config!(:idle_timeout_ms)

    status =
      case StateClient.get("/session/all") do
        {:ok, {:ok, sessions}} ->
          session_table = current_session_table()

          Enum.each(sessions, fn session ->
            :ets.insert_new(session_table, {session.token, session, Session.now(), idle_timeout})
          end)

          :ok

        {:error, reason} ->
          Logger.warn(
            "Could not fetch sessions from store, #{reason}, retrying in #{div(retry_ms, 1000)}s"
          )

          Process.send_after(self(), :retry_initialize, retry_ms)
          :unfetched
      end

    {:noreply, %{state | status: status}}
  end

  @impl true
  def handle_continue(:initialize, %__MODULE__{status: :ok} = state), do: {:noreply, state}

  defp get_status() do
    case :ets.whereis(:session_cache_table) do
      :undefined ->
        :uninitialized

      tid ->
        case :ets.lookup(tid, :current) do
          [] -> :uninitialized
          [{:current, _session_table, :unfetched, _created_at}] -> :unfetched
          [{:current, _session_table, _fetched_at, _created_at}] -> :ok
        end
    end
  end

  @impl true
  def handle_info(:retry_initialize, state), do: {:noreply, state, {:continue, :initialize}}

  @impl true
  def handle_info({:delete_from_store, token}, %__MODULE__{delete_fails: delete_fails} = state) do
    state =
      case StateClient.delete("/session", %{token: token}) do
        {:ok, :ok} ->
          state

        {:error, reason} ->
          Logger.warn("Could not delete token from the store, #{reason}")
          %{state | delete_fails: add_to_failure_list(delete_fails, token)}
      end

    {:noreply, state}
  end

  @impl true
  def handle_info({:put_in_store, session}, %__MODULE__{put_fails: put_fails} = state) do
    state =
      case StateClient.post("/session", %{session: session}) do
        {:ok, :ok} ->
          state

        {:error, reason} ->
          Logger.warn("Could not put token in store, #{reason}")
          %{state | put_fails: add_to_failure_list(put_fails, session)}
      end

    {:noreply, state}
  end

  @impl true
  def handle_info(:cleanup, %__MODULE__{status: :uninitialized} = state) do
    Logger.warn("Session cache cleanup called before initialization")
    cleanup_ms = get_config!(:cleanup_ms)
    Process.send_after(self(), :cleanup, cleanup_ms)
    {:noreply, state}
  end

  @impl true
  def handle_info(:cleanup, %__MODULE__{} = state) do
    %{put_fails: put_fails, delete_fails: delete_fails} = state
    cleanup_ms = get_config!(:cleanup_ms)

    put_fails = cleanup_fails(put_fails, &StateClient.post("/session", %{sessions: &1}))
    delete_fails = cleanup_fails(delete_fails, &StateClient.delete("/session", %{tokens: &1}))

    Process.send_after(self(), :cleanup, cleanup_ms)

    {:noreply, %{state | put_fails: put_fails, delete_fails: delete_fails}}
  end

  defp current_session_table() do
    with [{:current, table_name, _fetched_at, _created_at}] <-
           :ets.lookup(:session_cache_table, :current) do
      table_name
    end
  end

  defp apply_to_cache(operation) do
    with status when status in [:unfetched, :ok] <- get_status(),
         session_table <- current_session_table() do
      operation.(session_table)
    else
      _ -> raise "Session cache is uninitialized"
    end
  end

  defp generate_table_name() do
    unique_id = :crypto.strong_rand_bytes(24) |> Base.encode64()
    String.to_atom("session_cache_#{unique_id}")
  end

  defp cleanup_fails([], _cleanup_operation), do: []

  defp cleanup_fails(fails, cleanup_operation) when is_list(fails) do
    case cleanup_operation.(fails) do
      {:ok, :ok} ->
        []

      {:ok, {:error, fail_tokens}} ->
        fail_tokens = MapSet.new(fail_tokens)
        Enum.filter(fails, &MapSet.member?(fail_tokens, get_token(&1)))

      {:error, _reason} ->
        fails
    end
  end

  defp add_to_failure_list(list, item) when is_list(list) do
    max_fail_size = get_config!(:max_fail_size)

    if length(list) < max_fail_size do
      [item | list]
    else
      Logger.warn("Removing failed fallback token #{get_token(item)}")
      [item | Enum.slice(list, 0, max_fail_size - 1)]
    end
  end

  defp get_token(%Session{} = session), do: session.token
  defp get_token(token) when is_binary(token), do: token

  defp retry_ets_operation(operation), do: retry_ets_operation(operation, 2)

  defp retry_ets_operation(operation, tries) when tries > 0 do
    try do
      operation.()
    rescue
      _e in ArgumentError -> retry_ets_operation(operation, tries - 1)
    end
  end

  defp retry_ets_operation(_operation, _tries),
    do: raise("Stepper session cache ETS operation failed all tries")

  defp get_config!(key) when is_atom(key) do
    [val] = get_config!([key])
    val
  end

  defp get_config!(keys) when is_list(keys) do
    Application.fetch_env!(:stepper_gui, __MODULE__)
    |> get_config!([], keys)
  end

  defp get_config!(env, acc, [key | rest]) do
    val = Keyword.fetch!(env, key)
    get_config!(env, [val | acc], rest)
  end

  defp get_config!(_env, acc, []) do
    Enum.reverse(acc)
  end
end
