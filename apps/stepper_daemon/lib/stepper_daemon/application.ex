defmodule StepperDaemon.Application do
  @moduledoc false

  use Application

  require Logger

  alias Stepper.Unix.DomainSocket

  @impl true
  def start(_type, _args) do
    Logger.notice("Starting Stepper Daemon application...")
    socket_path = DomainSocket.prepare!(:daemon)

    children = [
      {StepperDaemonApi.Endpoint, http: [ip: {:local, socket_path}, port: 0]}
    ]

    opts = [strategy: :one_for_one, name: StepperDaemon.Supervisor]
    Logger.info("Starting application supervisor")
    sup = Supervisor.start_link(children, opts)

    if Phoenix.Endpoint.server?(:stepper_state, StepperStateApi.Endpoint) do
      Logger.info("Updating socket file permissions")
      DomainSocket.update_permissions!(socket_path)
    end

    Logger.notice("Stared Stepper Daemon")
    sup
  end
end
