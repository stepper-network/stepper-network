defmodule StepperDaemon.Unix.PamAuth do
  @on_load :load_pam_nif

  defp load_pam_nif do
    path = Application.app_dir(:stepper_daemon, "priv/pam_auth")
    :ok = :erlang.load_nif(to_charlist(path), 0)
  end

  @spec authenticate(binary, binary) :: :ok | {:error, atom}
  def authenticate(username, password) when is_binary(username) and is_binary(password) do
    pam_auth(to_charlist(username), to_charlist(password))
  end

  @spec pam_auth(charlist(), charlist()) :: :ok | {:error, atom}
  defp pam_auth(_username, _password) do
    raise "PAM NIF authenticate/2 not implemented"
  end
end
