#include <erl_nif.h>
#include <security/pam_appl.h>
#include <string.h>

// Converts PAM error code to Erlang atom.  Falls back to the integer if the code is unknown.
static ERL_NIF_TERM make_pam_error(ErlNifEnv *env, const int error_code)
{
  const char *pam_error;
  switch (error_code)
  {
  case 1:
    pam_error = "pam_open_err";
    break;
  case 2:
    pam_error = "pam_symbol_err";
    break;
  case 3:
    pam_error = "pam_service_err";
    break;
  case 4:
    pam_error = "pam_system_err";
    break;
  case 5:
    pam_error = "pam_buf_err";
    break;
  case 6:
    pam_error = "pam_perm_denied";
    break;
  case 7:
    pam_error = "pam_auth_err";
    break;
  case 8:
    pam_error = "pam_cred_insufficient";
    break;
  case 9:
    pam_error = "pam_authinfo_unavail";
    break;
  case 10:
    pam_error = "pam_user_unknown";
    break;
  case 11:
    pam_error = "pam_maxtries";
    break;
  case 12:
    pam_error = "pam_new_authtok_reqd";
    break;
  case 13:
    pam_error = "pam_acct_expired";
    break;
  case 14:
    pam_error = "pam_session_err";
    break;
  case 15:
    pam_error = "pam_cred_unavail";
    break;
  case 16:
    pam_error = "pam_cred_expired";
    break;
  case 17:
    pam_error = "pam_cred_err";
    break;
  case 18:
    pam_error = "pam_no_module_data";
    break;
  case 19:
    pam_error = "pam_conv_err";
    break;
  case 20:
    pam_error = "pam_authtok_err";
    break;
  case 21:
    pam_error = "pam_authtok_recovery_err";
    break;
  case 22:
    pam_error = "pam_authtok_lock_busy";
    break;
  case 23:
    pam_error = "pam_authtok_disable_aging";
    break;
  case 24:
    pam_error = "pam_try_again";
    break;
  case 25:
    pam_error = "pam_ignore";
    break;
  case 26:
    pam_error = "pam_abort";
    break;
  case 27:
    pam_error = "pam_authtok_expired";
    break;
  case 28:
    pam_error = "pam_module_unknown";
    break;
  case 29:
    pam_error = "pam_bad_item";
    break;
  case 30:
    pam_error = "pam_conv_again";
    break;
  case 31:
    pam_error = "pam_incomplete";
    break;
  default:
    return enif_make_int(env, error_code);
  }

  return enif_make_atom(env, pam_error);
}

// PAM conversation function for giving the password to the prompt.
// See: https://linux.die.net/man/3/pam_conv
static int converse(int num_msg,
                    const struct pam_message **msg,
                    struct pam_response **resp,
                    void *password)
{
  if (num_msg != 1)
    return PAM_CONV_ERR;

  int msg_style = msg[0]->msg_style;
  if ((msg_style != PAM_PROMPT_ECHO_OFF) && (msg_style != PAM_PROMPT_ECHO_ON))
    return PAM_CONV_ERR;

  *resp = malloc(sizeof(struct pam_response));
  (*resp)[0].resp_retcode = 0;
  (*resp)[0].resp = strdup(password);

  return PAM_SUCCESS;
}

/*
Function to authorize with Unix PAM given a username and password.

The function arguments of (username, password) should be charlists.

Has a maximum username/password size of 2048.
*/
static ERL_NIF_TERM pam_auth(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
  const int MAX_SIZE = 2048;
  char user[MAX_SIZE];
  char password[MAX_SIZE];

  // extract the function args to strings (should be charlists)
  if (1 > enif_get_string(env, argv[0], user, MAX_SIZE, ERL_NIF_LATIN1))
    return enif_make_badarg(env);
  if (1 > enif_get_string(env, argv[1], password, MAX_SIZE, ERL_NIF_LATIN1))
    return enif_make_badarg(env);

  // verify that the user/password can authenticate successfully
  struct pam_conv conv = {converse, password};
  pam_handle_t *pamh = NULL;
  int rc = pam_start("stepperd", user, &conv, &pamh);
  if (rc == PAM_SUCCESS)
    rc = pam_authenticate(pamh, 0);
  if (rc == PAM_SUCCESS)
    rc = pam_acct_mgmt(pamh, 0);
  pam_end(pamh, rc);

  if (rc != PAM_SUCCESS)
    return enif_make_tuple2(env, enif_make_atom(env, "error"), make_pam_error(env, rc));

  return enif_make_atom(env, "ok");
}

static ErlNifFunc nif_funcs[] = {
    {"pam_auth", 2, pam_auth}};

ERL_NIF_INIT(Elixir.StepperDaemon.Unix.PamAuth, nif_funcs, NULL, NULL, NULL, NULL)
