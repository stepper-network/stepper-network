defmodule StepperDaemon.System do
  @moduledoc """
  Defines system access functions used by the Stepper daemon.
  """

  alias StepperDaemon.Unix.PamAuth

  def auth(username, password) do
    PamAuth.authenticate(username, password)
  end
end
