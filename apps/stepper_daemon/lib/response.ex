defmodule StepperDaemon.Response do
  require Logger

  def encode_result(result) do
    case result do
      {:ok, result} ->
        {200, :erlang.term_to_binary(result)}

      {:error, msg} ->
        Logger.error(msg)
        {500, "server error"}
    end
  end
end
