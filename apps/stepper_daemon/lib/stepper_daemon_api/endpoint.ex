defmodule StepperDaemonApi.Endpoint do
  use Phoenix.Endpoint, otp_app: :stepper_daemon

  require Logger

  @impl true
  def init(:supervisor, config) do
    {:ok, config}
  end

  if code_reloading? do
    plug(Phoenix.CodeReloader)
  end

  plug(Plug.RequestId)
  plug(Plug.Telemetry, event_prefix: [:phoenix, :endpoint])

  plug(Plug.Parsers,
    parsers: [Stepper.Plug.Parsers.BinaryToTerm],
    pass: ["application/octet-stream"]
  )

  plug(Plug.MethodOverride)
  plug(Plug.Head)
  plug(StepperDaemonApi.Router)
end
