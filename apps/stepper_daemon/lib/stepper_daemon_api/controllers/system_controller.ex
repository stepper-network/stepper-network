defmodule StepperDaemonApi.SystemController do
  use StepperDaemonApi, :controller

  require Logger

  alias StepperDaemon.System

  def auth(conn, %{username: username, password: password}) do
    case System.auth(username, password) do
      :ok ->
        send_api_resp(conn, {:ok, :authorized})

      {:error, pam_err} ->
        Logger.notice("Failed login attempt for #{username}, #{pam_err}")
        send_api_resp(conn, {:error, :unauthorized})
    end
  end
end
