defmodule StepperDaemonApi.Router do
  use StepperDaemonApi, :router

  pipeline :api do
    plug(:accepts, ["octet-stream"])
  end

  scope "/", StepperDaemonApi do
    pipe_through([:api])

    post("/system/auth", SystemController, :auth)
  end
end
