# Stepper Daemon

The Stepper daemon is a low-level service run as root to control the underlying NixOS operating system.  It is only accessible through a unix domain socket which is defaulted to the path `/var/run/stepper/daemon.socket` with permissions `root:stepperd 660`.

It operates a Phoenix HTTP API through the socket.  Run `mix phx.routes StepperDaemonApi.Router` from within the `apps/stepper_daemon` directory to see all available routes.

## Motivation

Stepper has many opeations that generally require root access, like checking system passwords.  This required separation of services since we don't want a web GUI running as root.  Stepper Daemon evolved as a solution by running an isolated service as root to handle all 

## Development

As always, make sure to run `nix-shell` in the root of the application directory to get the proper environment.

Stepper Daemon will run as non-root, however, some functionality will not work and return errors:
  - PAM authentication (users can't log in)

Needing to run as root for full development has the consequence that the Nix application must be installed in a multi-user environment.  I bring this up because Fedora 36ish does not support this installation type of Nix (rather the folks at Nix do not support SELinux being enabled and I didn't feel like disabling it).  This may not be a problem in your flavor of Linux, but I'm not going on another distro hunt when everything works well now.  As a result, to test I run a NixOS GUI installation in a VM and develop on that.  I installed the thing manually with the Gnome desktop, no fancy `nixos-generate` stuff.  That way all of the Nix stuff just working out of the box and I can production test if needed using VM snapshots as easy fallbacks when everything breaks.

Simply run `sudo -E -u root -g users mix phx.server` to run the daemon locally with all functionality.

