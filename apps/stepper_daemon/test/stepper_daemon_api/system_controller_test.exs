defmodule StepperDaemonApi.SystemControllerTest do
  use StepperDaemonApi.ConnCase

  alias Stepper.Api.Request

  @username "bob"
  @password "bobspassword"
  @auth %{username: @username, password: @password}

  describe "auth" do
    test "successful", %{conn: conn} do
      Mimic.stub(StepperDaemon.System, :auth, fn _, _ -> :ok end)

      conn = post(conn, ~p"/system/auth", Request.encode!(@auth))
      assert api_response(conn, 200) == {:ok, :authorized}
    end

    test "failure", %{conn: conn} do
      Mimic.stub(StepperDaemon.System, :auth, fn _, _ -> {:error, :test} end)

      conn = post(conn, ~p"/system/auth", Request.encode!(@auth))
      assert api_response(conn, 200) == {:error, :unauthorized}
    end
  end
end
