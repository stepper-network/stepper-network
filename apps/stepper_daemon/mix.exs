defmodule Mix.Tasks.Compile.StepperDaemonNifs do
  def run(_args) do
    {result, error_code} = System.cmd("make", [], stderr_to_stdout: true, cd: __DIR__)

    if error_code == 0 do
      :ok
    else
      IO.puts("Error making Stepper Daemon nifs:")
      IO.binwrite(result)
      {:error, "Could not make Stepper Daemon nifs"}
    end
  end
end

defmodule StepperDaemon.MixProject do
  use Mix.Project

  def project do
    [
      app: :stepper_daemon,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      compilers: [:stepper_daemon_nifs] ++ Mix.compilers(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {StepperDaemon.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:mimic, "~> 1.7", only: :test},
      {:phoenix, "~> 1.7.0-rc.3", override: true},
      {:plug, "~> 1.14"},
      {:plug_cowboy, "~> 2.6"},
      {:stepper, in_umbrella: true},
      {:telemetry, "~> 1.1"}
    ]
  end
end
