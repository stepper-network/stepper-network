{
  description = "Stepper Network Applications";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.11";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, nixos-generators, ... }:
    let
      pname = "stepper-network";
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      serviceDir = "/var/lib/stepper";
      cookieFile = "${serviceDir}/secrets/.erlang.cookie";
    in {
      packages.${system} = {
        # Default is the stepper-network package containing all stepper nodes.
        # Currently just building the local git repo, should eventually be a git commit
        default = pkgs.callPackage ./nix/dev/default.nix { inherit pname; };

        # Creates a development VM with nixos-generators.  See README for usage.
        dev-vm = nixos-generators.nixosGenerate {
          system = system;
          modules = [
            ({ config, pkgs, lib, ... }: {
              imports = [ self.nixosModules.services ];
              config.services.stepper-network.enable = true;
            })
            ./nix/dev/configuration.nix
          ];
          format = "qcow";
        };
      };

      nixosModules.services = { config, lib, pkgs, options, ... }: {
        options.services.stepper-network.enable = lib.mkEnableOption "enable Stepper Network services";

        config = lib.mkIf config.services.stepper-network.enable {
          # create stepperd service
          systemd.services = {
            stepper-setup = {
              description = "Stepper Network System Setup";
              wantedBy = [ "stepperd.service" ];
              requiredBy = [ "stepperd.service" ];
              before = [ "stepperd.service" ];
              serviceConfig = {
                Type = "oneshot";
                RemainAfterExit = true;
              };
              script = ''
                set -e

                # create the stepper data directory
                mkdir -p --mode 775 "${serviceDir}"
                chown stepper:stepper "${serviceDir}"

                # create the Stepper run directory
                mkdir -p --mode 775 /var/run/stepper
                chown stepper:stepper /var/run/stepper

                # create the secrets directory
                SECRETS_DIR=${serviceDir}/secrets
                mkdir -p --mode 700 "$SECRETS_DIR"
                chown stepper:stepper "$SECRETS_DIR"

                # make an Erlang cookie file if one hasn't been created
                #if [ ! -f "${cookieFile}" ]; then
                  # create a cookie file with correct permissions and owner
                #  umask 177
                #  LC_ALL=C tr -dc A-Za-z0-9 < /dev/urandom | head -c 40 | base64 > "${cookieFile}"
                #  chown stepper:stepper "${cookieFile}"
                #fi
              '';
            };

            stepperd = {
              wantedBy = [ "multi-user.target" ];
              serviceConfig = {
                Restart = "always";
                ExecStart = "${self.packages.${system}.default}/bin/stepper_daemon start";
                ExecStop = "${self.packages.${system}.default}/bin/stepper_daemon stop";
              };
            };
          };

          # create the Stepper Network application groups
          users.groups = {
            stepper = { };
            stepperd = { };
          };

          users.users = {
            stepperd = {
              isSystemUser = true;
              group = "stepperd";
              extraGroups = [ "stepper" ];
            };

            stepper = {
              isSystemUser = true;
              group = "stepper";
              extraGroups = [ "stepperd" ]
            };
          };
        };
      };
    };
}